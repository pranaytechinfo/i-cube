### STAGE 1: Build ###
FROM node:10.19.0-alpine AS build
WORKDIR /usr/src/app
COPY package.json ./
#COPY package-lock.json ./
RUN npm install
COPY . ./
RUN npm run build --prod

### STAGE 2: Run ###
FROM nginx:1.23.1-alpine
COPY --from=build /usr/src/app/dist/fortune /usr/share/nginx/html/.
#COPY --from=build /usr/src/app/dist/fortune /usr/share/nginx/html/journal-recommendations/.

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

