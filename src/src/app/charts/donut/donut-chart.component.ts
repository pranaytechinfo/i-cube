import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import * as Highcharts from 'highcharts';
import Variablepie from "highcharts/modules/variable-pie";
Variablepie(Highcharts);
import HC_exporting from "highcharts/modules/exporting";
import offlineExporting from "highcharts/modules/offline-exporting";
import { environment } from "src/environments/environment.prod";
HC_exporting(Highcharts);
offlineExporting(Highcharts);


@Component({
    selector: 'app-donut-chart',
    templateUrl: './donut-chart.component.html',
})
export class DonutChartComponent implements AfterViewInit, OnChanges {
    @Input() chartData;
    @Input() chartId;
    data: any;

    ngAfterViewInit() {
        this.initDonutChart();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('chartData') && !changes.chartData.firstChange) {
            this.chartData = changes.chartData.currentValue;
            this.initDonutChart();
        }

    }

    initDonutChart() {
        this.data = {
            "chart": {
                "plotBackgroundColor": null,
                "plotBorderWidth": null,
                "plotShadow": false,
                "type": "pie"
            },
            variablepie: {
                dataLabels: {
                    style: {
                        width: '100px',
                        height: '500px'
                    }
                }
            },
            exporting: {
        enabled:environment.app.barActionButtonStatus,
        fallbackToExportServer: false,
                buttons: {
                    contextButton: {
                        menuItems: ["printChart", "separator", "downloadPNG", "downloadJPEG", "downloadSVG"],
                    },
                }
            },
            colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                return {
                    radialGradient: {
                        cx: 0.5,
                        cy: 0.3,
                        r: 0.7
                    },
                    stops: [
                        [0, color],
                        [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
                    ]
                };
            }),
            "title": {
                "text": this.chartData['title']
            },
            "tooltip": {
                "headerFormat": "",
                "pointFormat": "{point.y}"
            },
            credits: {
                enabled: false
            },
            "series": this.chartData['series']
            // {
            //     "minPointSize": 100,
            //     "zMin": 0,

        }
        Highcharts.chart(this.chartId, this.data);
    }
}
