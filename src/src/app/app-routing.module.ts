import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { ForgetComponent } from './auth/forget/forget.component';
import { MasterComponent } from './master/master.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';
import { MasterGuard } from './master/master.guard';

import { DashboardComponent } from './master/dashboard/dashboard.component';
import { PatentsComponent } from './master/modules/patents/patents.component';
import { GlobalSearchComponent } from './master/global-search/global-search.component';
import { ExecutiveSummaryComponent } from './master/summary/executive-summary/executive-summary.component';

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "auth/login" },
  {
    path: 'auth',
    component: AuthComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'forget', component: ForgetComponent },
      { path: 'logout', canActivate: [MasterGuard], component: LogoutComponent }
    ]
  },
  { path: 'resetPassword', component: ResetPasswordComponent },

  {
    path: 'projects',
    component: MasterComponent,
    canActivate: [MasterGuard],
    children: [
      {
        path: ':clientId/dashboard',
        component: DashboardComponent
      },
      {
        path: ':clientId/search',
        component: GlobalSearchComponent
      },
      {
        path: ':clientId/type/:id',
        component: PatentsComponent
      },
      {
        path: ':clientId/executive-summary',
        component: ExecutiveSummaryComponent
      }

    ]

  }



];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: false, // <-- debugging purposes only
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
