import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-multi-dropdown',
  templateUrl: './multi-dropdown.component.html',
  styleUrls: ['./multi-dropdown.component.scss']
})
export class MultiDropdownComponent implements OnInit, OnChanges {
  @Input() list;
  @Input() mainKey;
  @Input() selectedTerms = [];
  @Output() selectedList: EventEmitter<any> = new EventEmitter<any>();

  dropdownList = [];
  dropdownSettings = {};



  constructor() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          // case 'resetBool': {
          //   this.selectedTerms = [];
          // }
          case 'list' : {

            this.list.forEach(element => {

              element.display = element.key + ' (' + element.doc_count + ')'
              
            });

          }
      
        }
      }
    }

  }

  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'key',
      textField: 'display',
      itemsShowLimit: 7,
      allowSearchFilter: true,
      enableCheckAll: false,
      closeDropDownOnSelection: false
    };
  }

  onItemSelect(item: any) {
      this.selectedList.emit(this.selectedTerms);
  
  }
  onItemDeSelect(item: any) {
      this.selectedList.emit(this.selectedTerms);
 
  }


}
