import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() receiveTotalItems: any;
  @Input() resetBool: any;
  @Output() sendProps = new EventEmitter<object>();

  currentPage: number = 1;
  currentPageCopy: number = 1;
  totalPages: number;
  pageOffset: number = 10;
  start: number = 0;
  end: number = 0;

  constructor() { }

  ngOnInit() { }



  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'receiveTotalItems': {
            this.setPageLogic()
          }
          case 'resetBool': {
            this.currentPage = 1;
            this.currentPageCopy = 1;
            this.start = 0;
            this.end = 0;
          }
        }
      }
    }

  }

  setPageLogic() {
    this.totalPages = Math.ceil(this.receiveTotalItems / this.pageOffset);
  }

  onGoPage(event) {
    event.stopPropagation();
    let page = this.currentPage;
    if (this.isNumber(page)) {
      if (page >= 1 && page <= this.totalPages) {
        this.start = ((page * this.pageOffset) - this.pageOffset);
        if (this.start == 0) {
          this.start = 0
        }
        this.currentPageCopy = +this.currentPage;
        //send to parent
      } else {
        this.currentPage = +this.currentPageCopy;
      }
    } else {
      this.currentPage = +this.currentPageCopy;
    }
    this.lastNumberOfPage();
  }

  lastNumberOfPage() {
    if (this.receiveTotalItems >= (this.currentPage * 10)) {
      this.end = this.currentPage * 10;
    } else {
      this.end = this.receiveTotalItems;
    }
    this.sendProps.emit({ start: this.start, end: this.end, offset: this.pageOffset });
  }

  isNumber(text) {
    return /^\d+$/.test(text);
  }

  next(event) {
    event.stopPropagation();
    this.start = this.start + this.pageOffset;
    this.currentPage = +this.currentPage + 1;
    this.currentPageCopy = +this.currentPage;
    //send to parent
    this.lastNumberOfPage();
  }

  prev(event) {
    event.stopPropagation();
    this.start = this.start - this.pageOffset;
    this.currentPage = +this.currentPage - 1;
    this.currentPageCopy = +this.currentPage;
    //send to parent
    this.lastNumberOfPage();
  }

}
