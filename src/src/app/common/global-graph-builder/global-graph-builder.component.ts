import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { GlobalSearchService } from 'src/app/master/global-search/global-search.service';
import { ToastrService } from 'ngx-toastr';
import { General } from 'src/app/util/common/general';

@Component({
  selector: 'app-global-graph-builder',
  templateUrl: './global-graph-builder.component.html',
  styleUrls: ['./global-graph-builder.component.scss']
})
export class GlobalGraphBuilderComponent implements OnInit {
  @Input() clientId;
  @Input() boolPreloaderGraph;
  @Input() graphData;

  @Output() sendGraphClick = new EventEmitter<object>();
  @Output() onGraphBuild = new EventEmitter<object>();
  @Output() onGraphReset = new EventEmitter<object>();

  graphForm: FormGroup;

  dataHeaders: any[] = [];

  submitted: boolean = false;

  constructor(
    private _toastr: ToastrService,
    private _general: General,
    private _globalService: GlobalSearchService,
    private _formBuilder: FormBuilder) {
  }

  get f() {
    return this.graphForm.controls;
  }

  ngOnInit() {

    this.globalHeaders();
    this.resetForm();
  }

  resetForm() {
    this.graphForm = this._formBuilder.group({
      graphType: ['', [Validators.required]]
    });
  }


  onGraphBuilderSubmit(config) {
    this.onGraphBuild.emit(config);
  }


  sendOutGraphClick(data) {
    this.sendGraphClick.emit(data);
  }

  onReset() {
    this.graphForm.reset();

    this.onGraphReset.emit({});
  }
  dataReset(event){

    this.graphData={}
  }

  globalHeaders() {

    this.dataHeaders = [];

    this._globalService.getGlobalHeaders(this.clientId).subscribe((res: any) => {


      this.dataHeaders = res.result;


    }, err => {
      console.log(err);
      this._toastr.error('', this._general.getErrorMsg(err));
    })
  }


}
