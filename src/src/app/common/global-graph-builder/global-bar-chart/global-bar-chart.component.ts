

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from "highcharts/modules/exporting";
import offlineExporting from "highcharts/modules/offline-exporting";
import { environment } from 'src/environments/environment.prod';
HC_exporting(Highcharts);
offlineExporting(Highcharts);

@Component({
  selector: 'app-global-bar-chart',
  templateUrl: './global-bar-chart.component.html',
  styleUrls: ['./global-bar-chart.component.scss']
})
export class GlobalBarChartComponent implements OnInit {
  @Input() chartData;
  graphWidth:any=null;
  @Input() graphType;



  @Output() sendGraphClick = new EventEmitter<object>();

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {};

  noRecords: boolean = false;

  ngOnInit(): void {
    if (this.chartData && this.chartData.categories) {
      if((this.graphType==='bar')&&this.chartData.config.dataValues.length>25){
      this.graphWidth = this.chartData.config.dataValues.length*50;
      }
      this.initColumnChart();
    } else {
      this.noRecords = true;
    }
  }

  initColumnChart() {
   let opt = {

      "chart": {
        "type": "column",
       "width": this.graphWidth
      },
     "title": {
        "text": null
      },
      "tooltip": {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        useHTML: true,
        shared: false

      },
      exporting: {
        enabled: environment.app.barActionButtonStatus,
        fallbackToExportServer: false,
        buttons: {
          contextButton: {
            menuItems: ["printChart", "separator", "downloadPNG", "downloadJPEG", "downloadSVG"],
          },
        }
      },
      colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
          radialGradient: {
            cx: 0.5,
            cy: 0.3,
            r: 0.7
          },
          stops: [
            [0, color],
            [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
          ]
        };
      }),
      "xAxis": {
        "categories": this.chartData.categories,
        "crosshair": true,

      },
      "yAxis": {
        "min": 0,
         "title": {
          "text": null
        },

      },
      "plotOptions": {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            inside: false,
            // verticalAlign: 'top'
            // crop: false,
            //     overflow: 'none',
            formatter: function () {
              return (this.y != 0) ? this.y : "";
            }
          }

        },
        series: {
          events: {
            click: (event: any) => {
              this.clickBars(event);
            },
          }
        },


      },
      "credits": {
        "enabled": false
      },
      "series": this.chartData.series,
    }


    this.chartOptions = opt;
  }

  clickBars(eve) {


    let out = {}
    out[this.chartData.config.xaxisField] = eve.point.series.name;
    out['dataType'] = eve.point.category;


    this.sendGraphClick.emit(out)
  }

}
