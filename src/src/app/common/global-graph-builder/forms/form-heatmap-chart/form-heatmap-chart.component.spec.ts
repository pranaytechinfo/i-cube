import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormHeatmapChartComponent } from './form-heatmap-chart.component';

describe('FormHeatmapChartComponent', () => {
  let component: FormHeatmapChartComponent;
  let fixture: ComponentFixture<FormHeatmapChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormHeatmapChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormHeatmapChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
