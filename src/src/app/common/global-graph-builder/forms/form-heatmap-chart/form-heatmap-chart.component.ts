import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalSearchService } from 'src/app/master/global-search/global-search.service';
import { ToastrService } from 'ngx-toastr';
import { General } from 'src/app/util/common/general';

@Component({
  selector: 'app-form-heatmap-chart',
  templateUrl: './form-heatmap-chart.component.html',
  styleUrls: ['./form-heatmap-chart.component.scss']
})
export class FormHeatmapChartComponent implements OnInit {
  @Input() dataHeaders;
  @Input() clientId;
  @Input() typeChart;
  @Input() graphType;

  @Input() field1PlaceHolder;
  @Input() field2PlaceHolder;
  @Input() boolPreloaderGraph;
  @Output() onGraphBuild = new EventEmitter<object>();
  @Output() onGraphReset = new EventEmitter<object>();

  graphForm: FormGroup;

  dataAutosuggestListMulti1: any[] = [];
  dataAutosuggestListMulti2: any[] = [];
  boolPreloaderAutosuggestMulti1: boolean = false;
  boolPreloaderAutosuggestMulti2: boolean = false;


  extraItemSection: any[] = []
  dataAutosuggestList: any[] = [];
  topDataAutosuggestListM1: any[] = [];
  topDataAutosuggestListM2: any[] = [];

  selectedPrefixChoice: any = 0
  prefixSelctors = [{ value: 5, label: "Top 5" }, { value: 10, label: "Top 10" }, { value: 15, label: "Top 15" }, { value: 50, label: "Top 50" }, { value: 0, label: "Reset" },
    // { value: 1, label: "Select All" },
  ]
  prefixSelctorsY = [{ value: 5, label: "Top 5" }, { value: 10, label: "Top 10" }, { value: 15, label: "Top 15" }, { value: 20, label: "Top 20" }, { value: 0, label: "Reset" },
    // { value: 1, label: "Select All" },
  ]

  submitted: boolean = false;
  dropdownSettings: any = {};

  constructor(
    private _toastr: ToastrService,
    private _general: General,
    private _globalService: GlobalSearchService,
    private _formBuilder: FormBuilder) {


  }

  get f() {
    return this.graphForm.controls;
  }

  ngOnInit() {

    if (this.graphType==='BubbleChart') {
       this.prefixSelctors[3] = { value: 20, label: "Top 20" }
    }
    this.resetForm();

    this.dropdownSettings = {
      singleSelection: false,
      idField: '',
      textField: '',
      enableCheckAll: false,
      itemsShowLimit: 2,
      allowSearchFilter: true
    };
  }

  resetForm() {
    this.graphForm = this._formBuilder.group({
      graphField1: ['', [Validators.required]],
      fieldValues1: ['', [Validators.required]],
      graphField2: ['', [Validators.required]],
      fieldValues2: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.submitted = true;


    // stop here if form is invalid
    if (this.graphForm.invalid) {
      return;
    }

    if ((this.graphType==='HeatMap'||this.graphType==='BubbleChart')&&this.graphForm.value.fieldValues2.length > 20) {
      this._toastr.warning('', 'Field 2 options should be less than or equal to 20');
      return;

    }

    if ((this.graphType==='HeatMap')&&this.graphForm.value.fieldValues1.length > 50) {
      this._toastr.warning('', 'Field 1 options should be less than or equal to 50');
      return;

    }

    if ((this.graphType==='BubbleChart')&&this.graphForm.value.fieldValues1.length > 20) {
      this._toastr.warning('', 'Field 1 options should be less than or equal to 20');
      return;

    }

    let config = {
      "graphType": this.typeChart,
      "xaxisField": this.graphForm.value.graphField1,
      "xdataValues": this.graphForm.value.fieldValues1,
      "yaxisField": this.graphForm.value.graphField2,
      "ydataValues": this.graphForm.value.fieldValues2,
    }

    this.onGraphBuild.emit(config);

  }

  onReset() {
    this.onGraphReset.emit({});
  }

  //Heat map
  onField1Change() {
    let graphField = this.graphForm.value.graphField1;

    this.graphForm.controls["fieldValues1"].setValue("");

    if (!graphField) return;

    this.boolPreloaderAutosuggestMulti1 = true;
    this.dataAutosuggestListMulti1 = [];

    this._globalService.getGlobalAutosuggest(this.clientId, graphField).subscribe((res: any) => {


      if (res.result.length && res.result.length > 0) {


        if (this.graphType==='BubbleChart') {
          [5, 10, 15, 20].map((value) => {
            this.topDataAutosuggestListM1[value] = res.result.slice(0, Number(value));
          })
       }else{
        [5, 10, 15, 50].map((value) => {
          this.topDataAutosuggestListM1[value] = res.result.slice(0, Number(value));
        })
      }
      }


      this.boolPreloaderAutosuggestMulti1 = false;

      this.dataAutosuggestListMulti1 = res.result;


    }, err => {
      console.log(err);
      this.boolPreloaderAutosuggestMulti1 = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })

  }

  //Heat Map
  onField2Change() {
    let graphField = this.graphForm.value.graphField2;

    this.graphForm.controls["fieldValues2"].setValue("");

    if (!graphField) return;

    this.boolPreloaderAutosuggestMulti2 = true;
    this.dataAutosuggestListMulti2 = [];

    this._globalService.getGlobalAutosuggest(this.clientId, graphField).subscribe((res: any) => {

      if (res.result.length && res.result.length > 0) {

        [5, 10, 15, 20].map((value) => {
          this.topDataAutosuggestListM2[value] = res.result.slice(0, Number(value));
        })
      }

      this.boolPreloaderAutosuggestMulti2 = false;

      this.dataAutosuggestListMulti2 = res.result;


    }, err => {
      console.log(err);
      this.boolPreloaderAutosuggestMulti2 = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })

  }



  onItemSelectController(event) {
    //  console.log(event);
    this.extraItemSection.push(event)
  }
  onItemDeSelectController(event) {

    if (this.extraItemSection.indexOf(event) != -1) {
      this.extraItemSection.splice(this.extraItemSection.indexOf(event), 1)
    }
  }


  selectTopItems(event, selectionType) {
    //console.log(event.target.value);

    try {
      let type = event.target.value;


      if (selectionType === "M1") {

        if (Number(type) !== 0 && Number(type) > 1) {

          let topprefixDataAutosuggestList = this.topDataAutosuggestListM1[Number(type)]
          // topprefixDataAutosuggestList =  [...topprefixDataAutosuggestList, ...this.extraItemSection]
          this.graphForm.patchValue({ fieldValues1: [...new Set(topprefixDataAutosuggestList)] })
          this.selectedPrefixChoice = Number(type)
          this._toastr.info('Selected Top' + type)
        }

        if (Number(type) === 1) {
          this.graphForm.patchValue({
            fieldValues1: this.dataAutosuggestListMulti1
          })
          this._toastr.info('Selected All');

        }
        if (Number(type) === 0) {
          this.graphForm.patchValue({
            fieldValues1: []
          })
          this.extraItemSection = []
          this.selectElement('Selectors1', 'Select')

        }

      } else if (selectionType === "M2") {

        if (Number(type) !== 0 && Number(type) > 1) {

          let topprefixDataAutosuggestList = this.topDataAutosuggestListM2[Number(type)]
          // topprefixDataAutosuggestList =  [...topprefixDataAutosuggestList, ...this.extraItemSection]
          this.graphForm.patchValue({ fieldValues2: [...new Set(topprefixDataAutosuggestList)] })
          this.selectedPrefixChoice = Number(type)
          this._toastr.info('Selected Top' + type)
        }

        if (Number(type) === 1) {
          this.graphForm.patchValue({
            fieldValues2: this.dataAutosuggestListMulti2
          })
          this._toastr.info('Selected All');

        }
        if (Number(type) === 0) {
          this.graphForm.patchValue({
            fieldValues2: []
          })
          this.extraItemSection = []
          this.selectElement('Selectors2', 'Select')

        }

      }

    } catch (err) {
      this._toastr.error('Something went wrong')

    }


  }

  selectElement(id, valueToSelect) {
    let element = document.getElementById(id);
    element['value'] = valueToSelect;
  }

}
