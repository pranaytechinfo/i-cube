import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-form-bar-year-chart',
  templateUrl: './form-bar-year-chart.component.html',
  styleUrls: ['./form-bar-year-chart.component.scss']
})
export class FormBarYearChartComponent implements OnInit {
  @Input() dataHeaders;
  @Input() clientId;
  @Input() boolPreloaderGraph;
  @Output() onGraphBuild = new EventEmitter<object>();
  @Output() onGraphReset = new EventEmitter<object>();

  graphForm: FormGroup;

  submitted: boolean = false;

  constructor(
    private _formBuilder: FormBuilder) {
  }

  get f() {
    return this.graphForm.controls;
  }

  ngOnInit() {
    this.resetForm();
  }

  resetForm() {
    this.graphForm = this._formBuilder.group({
      graphField: ['', [Validators.required]],
      graphRange: ['', [Validators.required]],
      graphDataPattern: ['', [Validators.required]],

    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.graphForm.invalid) {
      return;
    }

    let config = {
      "graphType": "barYear",
      "xaxisField": this.graphForm.value.graphField,
      "dateType": this.graphForm.value.graphDataPattern

    }

    let start = formatDate(this.graphForm.value.graphRange[0], 'yyyy-MM-dd', 'en-US');
    let end = formatDate(this.graphForm.value.graphRange[1], 'yyyy-MM-dd', 'en-US');

    config['startDate'] = start;
    config['endDate'] = end;


    this.onGraphBuild.emit(config);

  }

  onReset() {
    this.onGraphReset.emit({});
  }



}
