import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBarYearChartComponent } from './form-bar-year-chart.component';

describe('FormBarYearChartComponent', () => {
  let component: FormBarYearChartComponent;
  let fixture: ComponentFixture<FormBarYearChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBarYearChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBarYearChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
