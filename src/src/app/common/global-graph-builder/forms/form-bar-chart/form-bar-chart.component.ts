import { Component, Input, OnInit, Output, EventEmitter, AfterViewInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalSearchService } from 'src/app/master/global-search/global-search.service';
import { ToastrService } from 'ngx-toastr';
import { General } from 'src/app/util/common/general';
import { of, Subscription } from 'rxjs';

@Component({
  selector: 'app-form-bar-chart',
  templateUrl: './form-bar-chart.component.html',
  styleUrls: ['./form-bar-chart.component.scss']
})
export class FormBarChartComponent implements OnInit {
  @Input() dataHeaders;
  @Input() clientId;
  @Input() boolPreloaderGraph;
  @Output() onGraphBuild = new EventEmitter<object>();
  @Output() onGraphReset = new EventEmitter<object>();

  graphForm: FormGroup;



  boolPreloaderAutosuggest: boolean = false;

  submitted: boolean = false; z
  selectedItems = [];
  graphSubscription: Subscription

  extraItemSection: any[] = []
  dataAutosuggestList: any[] = [];
  topDataAutosuggestList: any[] = [];
  selectedPrefixChoice: any = 0
  prefixSelctors = [{ value: 5, label: "Top 5" }, { value: 10, label: "Top 10" }, { value: 15, label: "Top 15" }, { value: 50, label: "Top 50" },{ value: 0, label: "Reset" },
  // { value: 1, label: "Select All" },
]


  dropdownSettings: any = {
    singleSelection: false,
    idField: '',
    textField: '',
    enableCheckAll: false,
    itemsShowLimit: 2,
    allowSearchFilter: true,

  };

  constructor(
    private _toastr: ToastrService,
    private _general: General,
    private _globalService: GlobalSearchService,
    private _formBuilder: FormBuilder) {
    this.resetForm();



  }

  get f() {
    return this.graphForm.controls;
  }

  ngOnInit() { }




  resetForm() {
    this.graphForm = this._formBuilder.group({
      graphField: ['', [Validators.required]],
      fieldValues: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.graphForm.invalid) {
      return;
    }


    let config = {
      "graphType": "bar",
      "xaxisField": this.graphForm.value.graphField,
      "dataValues": this.graphForm.value.fieldValues
    }
 this.onGraphBuild.emit(config);

  }

  onReset() {
    this.onGraphReset.emit({});
  }

  //Bar chart
  onFieldChange() {
    let graphField = this.graphForm.value.graphField;

    this.graphForm.controls["fieldValues"].setValue("");

    if (!graphField) return;

    this.boolPreloaderAutosuggest = true;
    this.dataAutosuggestList = [];

    this.graphSubscription = this._globalService.getGlobalAutosuggest(this.clientId, graphField).subscribe((res: any) => {


      if (res.result.length && res.result.length > 0) {

        [5, 10, 15, 50].map((value) => {
          this.topDataAutosuggestList[value] = res.result.slice(0, Number(value));
        })
      }
      this.boolPreloaderAutosuggest = false;

      this.dataAutosuggestList = res.result

    }, err => {
      console.log(err);
      this.boolPreloaderAutosuggest = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })

  }



  onItemSelectController(event) {
    this.extraItemSection.push(event)
  }
  onItemDeSelectController(event) {

    if (this.extraItemSection.indexOf(event) != -1) {
      this.extraItemSection.splice(this.extraItemSection.indexOf(event), 1)
    }
  }


  selectTopItems(event) {
    try {
      let type = event.target.value;
      if (Number(type) !== 0 && Number(type) > 1) {

        let topprefixDataAutosuggestList = this.topDataAutosuggestList[Number(type)]
        // topprefixDataAutosuggestList =  [...topprefixDataAutosuggestList, ...this.extraItemSection]
        this.graphForm.patchValue({ fieldValues: [...new Set(topprefixDataAutosuggestList)] })
        this.selectedPrefixChoice = Number(type)
        this._toastr.info('Selected Top' + type)
      }
      if (Number(type) === 0) {
        this.graphForm.patchValue({
          fieldValues: []
        })
        this.extraItemSection = []
        let element = document.getElementById('Selectors');
        element['value'] = 'Select';

      }

      if (Number(type) === 1) {
        this.graphForm.patchValue({
          fieldValues: this.dataAutosuggestList
        })
        this._toastr.info('Selected All');
      }


    } catch (err) {
      this._toastr.error('Something went wrong')

    }


  }

}
