import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBarChartComponent } from './form-bar-chart.component';

describe('FormBarChartComponent', () => {
  let component: FormBarChartComponent;
  let fixture: ComponentFixture<FormBarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
