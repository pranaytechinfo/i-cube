import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalSearchService } from 'src/app/master/global-search/global-search.service';
import { ToastrService } from 'ngx-toastr';
import { General } from 'src/app/util/common/general';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-form-heatmap-year-chart',
  templateUrl: './form-heatmap-year-chart.component.html',
  styleUrls: ['./form-heatmap-year-chart.component.scss']
})
export class FormHeatmapYearChartComponent implements OnInit {
  @Input() dataHeaders;
  @Input() clientId;
  @Input() boolPreloaderGraph;
  @Output() onGraphBuild = new EventEmitter<object>();
  @Output() onGraphReset = new EventEmitter<object>();

  graphForm: FormGroup;

  dataAutosuggestListMulti2: any[] = [];
  boolPreloaderAutosuggestMulti2: boolean = false;

  submitted: boolean = false;
  dropdownSettings: any = {};


  extraItemSection: any[] = []
  dataAutosuggestList: any[] = [];
  topDataAutosuggestList: any[] = [];
  selectedPrefixChoice: any = 0
  prefixSelctors = [{ value: 5, label: "Top 5" }, { value: 10, label: "Top 10" }, { value: 15, label: "Top 15" }, { value: 20, label: "Top 20" },{ value: 0, label: "Reset" },
  // { value: 1, label: "Select All" },
]

  constructor(
    private _toastr: ToastrService,
    private _general: General,
    private _globalService: GlobalSearchService,
    private _formBuilder: FormBuilder) {
  }

  get f() {
    return this.graphForm.controls;
  }

  ngOnInit() {

    this.resetForm();

    this.dropdownSettings = {
      singleSelection: false,
      idField: '',
      textField: '',
      enableCheckAll: false,
      itemsShowLimit: 2,
      allowSearchFilter: true
    };
  }

  resetForm() {
    this.graphForm = this._formBuilder.group({
      graphYear1: ['', [Validators.required]],
      fieldRange1: ['', [Validators.required]],
      graphField2: ['', [Validators.required]],
      fieldValues2: ['', [Validators.required]],
      graphDataPattern: ['', [Validators.required]],

    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.graphForm.invalid) {
      return;
    }


    if (this.graphForm.value.fieldValues2.length > 20) {
      this._toastr.warning('', 'Field(Y-Axis) options should be less than or equal to 20');
      return
    }


    let config = {
      "graphType": "HeatMapYear",
      "xaxisField": this.graphForm.value.graphYear1,
      "yaxisField": this.graphForm.value.graphField2,
      "dataValues": this.graphForm.value.fieldValues2,
      "dateType": this.graphForm.value.graphDataPattern
    }

    let start = formatDate(this.graphForm.value.fieldRange1[0], 'yyyy-MM-dd', 'en-US');
    let end = formatDate(this.graphForm.value.fieldRange1[1], 'yyyy-MM-dd', 'en-US');

    config['startDate'] = start;
    config['endDate'] = end;

    this.onGraphBuild.emit(config);

  }

  onReset() {
    this.onGraphReset.emit({});
  }

  //Heat map
  onField2Change() {
    let graphField = this.graphForm.value.graphField2;

    this.graphForm.controls["fieldValues2"].setValue("");

    if (!graphField) return;

    this.boolPreloaderAutosuggestMulti2 = true;
    this.dataAutosuggestListMulti2 = [];

    this._globalService.getGlobalAutosuggest(this.clientId, graphField).subscribe((res: any) => {

      [5, 10, 15, 20].map((value) => {
        this.topDataAutosuggestList[value] = res.result.slice(0, Number(value));
      })
      this.boolPreloaderAutosuggestMulti2 = false;

      this.dataAutosuggestListMulti2 = res.result;


    }, err => {
      console.log(err);
      this.boolPreloaderAutosuggestMulti2 = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })

  }


  dataPatternSelection() {

  }


  onItemSelectController(event) {
   this.extraItemSection.push(event)
  }
  onItemDeSelectController(event) {

    if (this.extraItemSection.indexOf(event) != -1) {
      this.extraItemSection.splice(this.extraItemSection.indexOf(event), 1)
    }
  }


  selectTopItems(event) {


    try {
      let type = event.target.value;

      if (Number(type) !== 0 && Number(type) > 1) {

        let topprefixDataAutosuggestList = this.topDataAutosuggestList[Number(type)]
        // topprefixDataAutosuggestList =  [...topprefixDataAutosuggestList, ...this.extraItemSection]
        this.graphForm.patchValue({ fieldValues2: [...new Set(topprefixDataAutosuggestList)] })
        this.selectedPrefixChoice = Number(type)
        this._toastr.info('Selected Top' + type)
      }
      if (Number(type) === 0) {
        this.graphForm.patchValue({
          fieldValues2: []
        })
        this.extraItemSection = []
        let element = document.getElementById('Selectors');
        element['value'] = 'Select';
      }

        if (Number(type) === 1) {
          this.graphForm.patchValue({
            fieldValues2: this.dataAutosuggestListMulti2
          })
        this._toastr.info('Selected All');

        }




    } catch (err) {
      this._toastr.error('Something went wrong')

    }


  }



}
