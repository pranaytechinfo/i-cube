import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormHeatmapYearChartComponent } from './form-heatmap-year-chart.component';

describe('FormHeatmapYearChartComponent', () => {
  let component: FormHeatmapYearChartComponent;
  let fixture: ComponentFixture<FormHeatmapYearChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormHeatmapYearChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormHeatmapYearChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
