import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGraphBuilderComponent } from './global-graph-builder.component';

describe('GlobalGraphBuilderComponent', () => {
  let component: GlobalGraphBuilderComponent;
  let fixture: ComponentFixture<GlobalGraphBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGraphBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGraphBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
