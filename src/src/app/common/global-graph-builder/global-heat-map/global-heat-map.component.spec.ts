import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalHeatMapComponent } from './global-heat-map.component';

describe('GlobalHeatMapComponent', () => {
  let component: GlobalHeatMapComponent;
  let fixture: ComponentFixture<GlobalHeatMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalHeatMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalHeatMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
