import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from "highcharts/modules/exporting";
import offlineExporting from "highcharts/modules/offline-exporting";
import heatmap from "highcharts/modules/heatmap";
import { environment } from 'src/environments/environment.prod';

heatmap(Highcharts)
HC_exporting(Highcharts);
offlineExporting(Highcharts);

@Component({
  selector: 'app-global-heat-map',
  templateUrl: './global-heat-map.component.html',
  styleUrls: ['./global-heat-map.component.scss']
})
export class GlobalHeatMapComponent implements OnInit {
  @Input() chartData;
  @Input() graphType;


  @Output() sendGraphClick = new EventEmitter<object>();

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {};

  noRecords: boolean = false;
  graphWidth: any = null;
  graphHeight: any = null;


  ngOnInit(): void {
   // console.log(this.chartData)
    if (this.chartData && this.chartData.xAxis) {
      if ((this.graphType==='HeatMap')&&this.chartData.config.xdataValues.length > 25) {
        this.graphWidth = this.chartData.xAxis.length * 50;
      }
     // if (this.chartData.config.ydataValues.length > 25) {
        this.graphHeight = this.chartData.yAxis.length * 52;
      //}

      this.initColumnChart();
    } else {
      this.noRecords = true;
    }
  }

  initColumnChart() {
  console.log(this.graphHeight, this.graphWidth);

   if(this.graphHeight<300){
    this.graphHeight=null
   }

    let _self = this;

    let opt = {

      chart: {
        type: "heatmap",
        marginTop: 40,
        marginBottom: 80,
        plotBorderWidth: 1,
       "width": this.graphWidth,
        "height":  this.graphHeight

      },
      "title": {
        "text": null
      },
      exporting: {
        enabled: environment.app.barActionButtonStatus,

        fallbackToExportServer: false,
        buttons: {
          contextButton: {
            menuItems: ["printChart", "separator", "downloadPNG", "downloadJPEG", "downloadSVG"],
          },
        }
      },

      "xAxis": {
        "categories": this.chartData.xAxis,
      },
      yAxis: {
        categories: this.chartData.yAxis,
        title: null,
        reversed: true
      },

      colorAxis: {
        min: 0,
        minColor: '#FFFFFF',
        maxColor: Highcharts.getOptions().colors[3]
      },



      series: this.chartData.series,

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            yAxis: {
              labels: {
                formatter: function () {
                  return this.value.charAt(0);
                }
              }
            }
          }
        }]
      },



      accessibility: {
        point: {
          descriptionFormatter: function (point) {
            var ix = point.index + 1,
              xName = _self.getPointCategoryName(point, "x"),
              yName = _self.getPointCategoryName(point, "y"),
              val = point.value;
            return ix + ". " + xName + " sales " + yName + ", " + val + ".";
          }
        }
      },

      legend: {
        align: "right" as "right",
        layout: "vertical" as "vertical",
        margin: 0,
        verticalAlign: "top" as "top",
        y: 25,
        symbolHeight: 280
      },


      tooltip: {
        formatter: function () {
          return (
            "<b>" +
            _self.getPointCategoryName(this.point, "x") +
            "</b>  <br><b>" +
            _self.getPointCategoryName(this.point, "y") +

            "</b>  <br>Total Documents: <b> " +
            this.point.value +
            "</b>"

          );
        }
      },
      "plotOptions": {
        "series": {
          cursor: 'pointer',
          "dataLabels": {
            enabled: true,
            color: "#000000"

          },
          point: {
            events: {
              click: (event: any) => {
                this.clickBars(event);

              }
            }
          }
        }
      },

      "credits": {
        "enabled": false
      }


    }


    this.chartOptions = opt;
  }


  getPointCategoryName(point, dimension) {
    var series = point.series,
      isY = dimension === "y",
      axis = series[isY ? "yAxis" : "xAxis"];
    return axis.categories[point[isY ? "y" : "x"]];
  }

  clickBars(eve) {

    let out = {}
    out[this.chartData.config.xaxisField] = this.getPointCategoryName(eve.point, "x");
    out[this.chartData.config.yaxisField] = this.getPointCategoryName(eve.point, "y");

    this.sendGraphClick.emit(out)
  }

}
