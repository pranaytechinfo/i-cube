import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from "highcharts/modules/exporting";
import offlineExporting from "highcharts/modules/offline-exporting";
import HC_more from "highcharts/highcharts-more";
import { environment } from 'src/environments/environment.prod';

HC_more(Highcharts);
HC_exporting(Highcharts);
offlineExporting(Highcharts);

interface ExtendedChart extends Highcharts.PlotPackedbubbleOptions {
  layoutAlgorithm: {
    splitSeries: any;
  };
}

@Component({
  selector: 'app-global-bubble-chart',
  templateUrl: './global-bubble-chart.component.html',
  styleUrls: ['./global-bubble-chart.component.scss']
})
export class GlobalBubbleChartComponent implements OnInit {

  @Input() chartData;

  @Output() sendGraphClick = new EventEmitter<object>();

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {};

  noRecords: boolean = false;

  ngOnInit(): void {
    if (this.chartData && this.chartData.series) {
      this.initColumnChart();
    } else {
      this.noRecords = true;
    }
  }

  initColumnChart() {

    let _self = this;

    let opt = {

      chart: {
        type: 'packedbubble',
        height: '550px'
      },
      "title": {
        "text": null
      },
      exporting: {
        enabled:environment.app.barActionButtonStatus,
        fallbackToExportServer: false,
        buttons: {
          contextButton: {
            menuItems: ["printChart", "separator", "downloadPNG", "downloadJPEG", "downloadSVG"],
          },
        }
      },

      plotOptions: {
        packedbubble: {
          minSize: "20%",
          maxSize: "100%",
          zMin: 0,
          zMax: 1000,
          cursor: 'pointer',
          layoutAlgorithm: {
            gravitationalConstant: 0.05,
            splitSeries: true,
            seriesInteraction: false,
            dragBetweenSeries: true,
            parentNodeLimit: true
          },
          events: {
            click: (event: any) => {
              this.clickBars(event);

            }
          },
          dataLabels: {
            enabled: true,
            format: "{point.name}",
            style: {
              color: "black",
              textOutline: "none",
              fontWeight: "normal"
            }
          }
        } as ExtendedChart
      },

      series: this.chartData.series,

      tooltip: {
        useHTML: true,
        pointFormat: '<b>{point.name}:</b> {point.value}'
      },



      "credits": {
        "enabled": false
      }


    }


    this.chartOptions = opt;
  }




  clickBars(eve) {

    if(eve.point.name) {
      let out = {}

      out[this.chartData.config.xaxisField] = eve.point.series.name;
      out[this.chartData.config.yaxisField] = eve.point.name;

      this.sendGraphClick.emit(out)
    }

  }


}
