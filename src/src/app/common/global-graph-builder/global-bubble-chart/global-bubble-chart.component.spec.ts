import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalBubbleChartComponent } from './global-bubble-chart.component';

describe('GlobalBubbleChartComponent', () => {
  let component: GlobalBubbleChartComponent;
  let fixture: ComponentFixture<GlobalBubbleChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalBubbleChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalBubbleChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
