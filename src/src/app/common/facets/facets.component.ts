import { Component, Input, OnInit, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-facets',
  templateUrl: './facets.component.html',
  styleUrls: ['./facets.component.scss']
})
export class FacetsComponent implements OnInit, OnChanges {
  @Input() inputFacets;
  @Input() config:any[]=[];
  @Input() resetBool;

  @Output() sendSelFacets = new EventEmitter<object>();
  @Output() sendClickReset = new EventEmitter<object>();

  selectedFacets: any = {};
  selectedFullFacets: any = {};

  selectedDates: any = {};

  added: boolean = false;


  constructor() { }

  ngOnInit() {

  }


  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'resetBool': {

            this.selectedFullFacets = {};
            this.selectedFacets = {};
            this.selectedDates = {};

            this.added = false;

          }
        }
      }
    }

  }



  onAutoSuggestChange(value, type) {

    let arr = [];
    value.forEach(element => {
      arr.push(element.key)
    });

    this.selectedFacets[type] = arr;
    this.selectedFullFacets[type] = value;


  }

  onApplyFilter() {


    let selDates = {}

    Object.entries(this.selectedDates).forEach(([key, value]) => {

      let start = formatDate(value[0], 'yyyy-MM-dd', 'en-US');
      let end = formatDate(value[1], 'yyyy-MM-dd', 'en-US');

      selDates[key] =  { "start": start, "end": end }
    });



    this.sendSelFacets.emit({dateFacets: selDates, facets: this.selectedFacets});
    this.added = true;
  }

  onReset() {

    this.selectedFullFacets = {};
    this.selectedFacets = {};
    this.selectedDates = {};

    this.added = false;

    this.sendClickReset.emit(this.selectedFacets);

  }


}
