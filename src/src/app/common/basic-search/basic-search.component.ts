import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-basic-search',
  templateUrl: './basic-search.component.html',
  styleUrls: ['./basic-search.component.scss']
})
export class BasicSearchComponent implements OnInit {
  @Input() boolPreloader;
  @Input() placeholder;
  @Input() heading;
  @Output() sendBasicText = new EventEmitter<string>();
  @Output() sendClickMainReset = new EventEmitter<boolean>();
  selectSearchType: Boolean = false

  inputSearch: string = "";

  constructor(
    private toastr: ToastrService
  ) { }

  ngOnInit() {
  }

  onSearch() {
   let modifiedinputSearch = ''
    if (this.inputSearch) {
       if (this.selectSearchType) {
        modifiedinputSearch = '"' + this.inputSearch + '"'
      }else{
        modifiedinputSearch = this.inputSearch
      }
         this.sendBasicText.emit(modifiedinputSearch);
    } else {
      this.toastr.warning("Please enter search term")
    }

  }



  onReset() {
    this.inputSearch = "";
    this.sendClickMainReset.emit(true);

  }

  fieldsChange(values: any): void {
    this.selectSearchType = values.currentTarget.checked
  }

}
