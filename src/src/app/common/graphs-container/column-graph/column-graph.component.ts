import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from "highcharts/modules/exporting";
import offlineExporting from "highcharts/modules/offline-exporting";
import { environment } from 'src/environments/environment.prod';
HC_exporting(Highcharts);
offlineExporting(Highcharts);

@Component({
  selector: 'app-column-graph',
  templateUrl: './column-graph.component.html',
  styleUrls: ['./column-graph.component.scss']
})
export class ColumnGraphComponent implements OnInit {
  @Input() chartData;
  @Output() sendGraphClick = new EventEmitter<object>();

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {};

  noRecords: boolean = false;

  ngOnInit(): void {
    if (this.chartData && this.chartData.categories) {
      this.initColumnChart();
    } else {
      this.noRecords = true;
    }
  }

  initColumnChart() {



    let opt = {

      "chart": {
        "type": "column"
      },
      "title": {
        "text": null
      },
      "tooltip": {
        "pointFormat": "Count: {point.y}"
      // headerFormat: '<table>',
      //   pointFormat: '<table><tr>' +
      //     '<td style="padding:0"><b>{point.y}</b></td></tr>',
      //   footerFormat: '</table>',

      // },
          //   "tooltip": {
    //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
    //       '<td style="padding:0"><b>{point.y}</b></td></tr>',
    //     footerFormat: '</table>',
    //     useHTML: true
    },
      exporting: {
        enabled:environment.app.barActionButtonStatus,
        fallbackToExportServer: false,
        buttons: {
          contextButton: {
            menuItems: ["printChart", "separator", "downloadPNG", "downloadJPEG", "downloadSVG"],
          },
        }
      },
      "xAxis": {
        "categories": this.chartData.categories,
        "crosshair": true
      },
      "yAxis": {
        "min": 0,
        "title": {
          "text": this.chartData.yaxisDisplayName
        }
      },
      colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
          radialGradient: {
            cx: 0.5,
            cy: 0.3,
            r: 0.7
          },
          stops: [
            [0, color],
            [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
          ]
        };
      }),
      "plotOptions": {
        "series": {
          "colorByPoint": false,
          cursor: 'pointer',
          events: {
            click: (event: any) => this.clickBars(event),
          },
        },
        "column": {
          "pointPadding": 0.2,
          "borderWidth": 0,
          "dataLabels": {
            enabled: true,
            y: -25,
            verticalAlign: 'top'
          }
        }
      },
      "credits": {
        "enabled": false
      },
      "series": this.chartData.series,

    }

//For Multiple Data Set
    // let opt = {

    //   "chart": {
    //     "type": "column"
    //   },
    //   "title": {
    //     "text": null
    //   },
    //   "tooltip": {
    //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
    //       '<td style="padding:0"><b>{point.y}</b></td></tr>',
    //     footerFormat: '</table>',
    //     useHTML: true
    //   },
    //   exporting: {
    //     fallbackToExportServer: false,
    //     buttons: {
    //       contextButton: {
    //         menuItems: ["printChart", "separator", "downloadPNG", "downloadJPEG", "downloadSVG"],
    //       },
    //     }
    //   },
    //   colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
    //     return {
    //       radialGradient: {
    //         cx: 0.5,
    //         cy: 0.3,
    //         r: 0.7
    //       },
    //       stops: [
    //         [0, color],
    //         [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
    //       ]
    //     };
    //   }),
    //   "xAxis": {
    //     "categories": this.chartData.categories,
    //     "crosshair": true
    //   },
    //   "yAxis": {
    //     "min": 0,
    //     "title": {
    //       "text": null
    //     }
    //   },
    //   "plotOptions": {
    //     column: {
    //       pointPadding: 0.2,
    //       borderWidth: 0,
    //       cursor: 'pointer',
    //       "column": {
    //         "pointPadding": 0.2,
    //         "borderWidth": 0,
    //         "dataLabels": {
    //           enabled: true,
    //           y: -25,
    //           verticalAlign: 'top'
    //         }
    //       }
    //     },
    //     series: {
    //       "colorByPoint": true,
    //       events: {
    //         click: (event: any) => {
    //           this.clickBars(event);
    //         },
    //       }
    //     }
    //   },
    //   "credits": {
    //     "enabled": false
    //   },
    //   "series": this.chartData.series,

    // }


    this.chartOptions = opt;
  }

  clickBars(eve) {

    let out = {}
    out[this.chartData.xaxisField] = eve.point.category

    this.sendGraphClick.emit(out)
  }

}
