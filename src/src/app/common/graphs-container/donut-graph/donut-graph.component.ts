import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from "highcharts/modules/exporting";
import offlineExporting from "highcharts/modules/offline-exporting";
import { environment } from 'src/environments/environment.prod';
HC_exporting(Highcharts);
offlineExporting(Highcharts);

@Component({
  selector: 'app-donut-graph',
  templateUrl: './donut-graph.component.html',
  styleUrls: ['./donut-graph.component.scss']
})
export class DonutGraphComponent implements OnInit {
  @Input() chartData;
  @Output() sendGraphClick = new EventEmitter<object>();

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {};

  noRecords: boolean = false;

  constructor() { }


  ngOnInit(): void {

    if (this.chartData && this.chartData.series) {
      this.initColumnChart();
    } else {
      this.noRecords = true;
    }
  }

  initColumnChart() {



    let opt = {
      "chart": {
        "plotBackgroundColor": null,
        "plotBorderWidth": null,
        "plotShadow": false,
        "type": "pie"
      },
      variablepie: {
        dataLabels: {
          style: {
            width: '100px'
          }
        }
      },
      exporting: {
        enabled:environment.app.barActionButtonStatus,
        fallbackToExportServer: false,
        buttons: {
          contextButton: {
            menuItems: ["printChart", "separator", "downloadPNG", "downloadJPEG", "downloadSVG"],
          },
        }
      },
       colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
          radialGradient: {
            cx: 0.5,
            cy: 0.3,
            r: 0.7
          },
          stops: [
            [0, color],
            [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
          ]
        };
      }),
      "title": {
        "text": null
      },
      "tooltip": {
        "headerFormat": "",
        "pointFormat": "{point.y}"
      },
      credits: {
        enabled: false
      },
      "plotOptions": {
        "pie": {
          cursor: this.chartData.graphType == 'donut' ? 'pointer' : 'default',
          events: {
            click: (event: any) => this.clickBars(event),
          },
        },

      },
      "series": this.chartData.series
    }

    this.chartOptions = opt;
  }

  clickBars(eve) {
    let out = {}
    if (this.chartData.graphType == 'donut') {
      out[this.chartData.xaxisField] = eve.point.name

      this.sendGraphClick.emit(out)
    }

  }

}
