import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from "highcharts/modules/exporting";
import offlineExporting from "highcharts/modules/offline-exporting";
import { environment } from 'src/environments/environment.prod';
HC_exporting(Highcharts);
offlineExporting(Highcharts);

@Component({
  selector: 'app-bar-graph',
  templateUrl: './bar-graph.component.html',
  styleUrls: ['./bar-graph.component.scss']
})
export class BarGraphComponent implements OnInit {
  @Input() chartData;
  @Output() sendGraphClick = new EventEmitter<object>();

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {};

  noRecords: boolean = false;

  ngOnInit(): void {
    if (this.chartData && this.chartData.categories) {
      this.initColumnChart();
    } else {
      this.noRecords = true;
    }
  }

  initColumnChart() {

    let opt = {

      "chart": {
        "type": "bar"
      },
      "title": {
        "text": null
      },
      "tooltip": {
        "pointFormat": "Count: {point.y}"
      },
      "xAxis": {
        "categories": this.chartData.categories,
        "crosshair": true
      },
      "yAxis": {
        "min": 0,
        "title": {
          "text": this.chartData.yaxisDisplayName
        }
      },
      colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
          radialGradient: {
            cx: 0.5,
            cy: 0.3,
            r: 0.7
          },
          stops: [
            [0, color],
            [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
          ]
        };
      }),
      exporting: {
        enabled:environment.app.barActionButtonStatus,
        fallbackToExportServer: false,
        buttons: {
          contextButton: {
            menuItems: ["printChart", "separator", "downloadPNG", "downloadJPEG", "downloadSVG"],
          },
        }
      },
      "plotOptions": {
        "series": {
          "colorByPoint": false,
          cursor: 'pointer',
          events: {
            click: (event: any) => {
              this.clickBars(event);
            },
          },
        },
        "bar": {
          "pointPadding": 0.2,
          "borderWidth": 0,
          "dataLabels": {
            enabled: true,
            y: -7,
            verticalAlign: 'top'
          }
        }
      },
      "credits": {
        "enabled": false
      },
      "series": this.chartData.series,

    }


    this.chartOptions = opt;
  }

  clickBars(eve) {

    let out = {}
    out[this.chartData.xaxisField] = eve.point.category

    this.sendGraphClick.emit(out)
  }

}
