import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as Highcharts from 'highcharts/highmaps';
import worldMap from '@highcharts/map-collection/custom/world.geo.json';
import HC_exporting from "highcharts/modules/exporting";
import offlineExporting from "highcharts/modules/offline-exporting";
import { environment } from 'src/environments/environment.prod';
HC_exporting(Highcharts);
offlineExporting(Highcharts);

@Component({
  selector: 'app-map-graph',
  templateUrl: './map-graph.component.html',
  styleUrls: ['./map-graph.component.scss']
})
export class MapGraphComponent implements OnInit {
  @Input() chartData;
  @Output() sendGraphClick = new EventEmitter<object>();

  Highcharts: typeof Highcharts = Highcharts;
  chartConstructor = 'mapChart';
  chartOptions: Highcharts.Options

  noRecords: boolean = false;
  countrywithCodes:any[]=[]

  constructor() { }

  ngOnInit(): void {
    if (this.chartData && this.chartData.data) {

     this.countrywithCodes = this.chartData.countryAndCodes
      this.initColumnChart();
    } else {
      this.noRecords = true;
    }

  }

  initColumnChart() {

    this.chartOptions = {
      chart: {
        map: worldMap,
      },
      title: {
        text: null,
      },
      credits: {
        enabled: false
      },
      mapNavigation: {
        enabled: true,
        buttonOptions: {
          alignTo: 'spacingBox',
        },
      },
      exporting: {
        enabled:environment.app.barActionButtonStatus,
        fallbackToExportServer: false,
        buttons: {
          contextButton: {
            menuItems: ["printChart", "separator", "downloadPNG", "downloadJPEG", "downloadSVG"],
          },
        }
      },
      legend: {
        enabled: true,
      },
      colorAxis: {
        min: 0,
      },

      "plotOptions": {
        "map": {
          cursor: 'pointer',
          events: {
            click: (event: any) => this.clickBars(event),
          },
        }
      },
      series: [
        {
          type: 'map',
          name: '',
          states: {
            hover: {
              color: '#BADA55',
            },
          },
          dataLabels: {
            enabled: false,
            // format: '{point.name}',
          },
          allAreas: true,
          data: this.chartData['data']['val'],

        },
      ],
    };

  }

  clickBars(eve) {
    console.log(eve);

    let out = {}
    out[this.chartData.xaxisField] = eve.point.name;

    if (this.chartData.facetValue) {
      out[this.chartData.facetField] = this.chartData.facetValue;
    }

    this.sendGraphClick.emit(out)
  }

}
