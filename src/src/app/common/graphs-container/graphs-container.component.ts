import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-graphs-container',
  templateUrl: './graphs-container.component.html',
  styleUrls: ['./graphs-container.component.scss']
})
export class GraphsContainerComponent implements OnInit {
  @Input() graphData;
  @Input() boolPreloader;
  @Output() sendGraphClick = new EventEmitter<object>();

  constructor(
  ) {
    console.log(this.graphData);

  }

  ngOnInit() {
  }

  sendOutGraphClick(eve) {
    this.sendGraphClick.emit(eve)
  }


}
