import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-global-card',
  templateUrl: './global-card.component.html',
  styleUrls: ['./global-card.component.scss']
})
export class GlobalCardComponent implements OnInit {
  @Input() loading;
  @Input() records;
  @Input() highlightObj;
  @Input() projectConfigs;

  constructor(

  ) { }

  ngOnInit() {
  }


}
