import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-gloabl-list',
  templateUrl: './gloabl-list.component.html',
  styleUrls: ['./gloabl-list.component.scss']
})
export class GloablListComponent implements OnInit {
  @Input() loading;
  @Input() records;
  @Input() highlightObj;

  constructor(

  ) { }

  ngOnInit() {
  }

}
