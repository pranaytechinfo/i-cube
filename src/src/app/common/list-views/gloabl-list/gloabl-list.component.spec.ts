import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GloablListComponent } from './gloabl-list.component';

describe('GloablListComponent', () => {
  let component: GloablListComponent;
  let fixture: ComponentFixture<GloablListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GloablListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GloablListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
