import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPatentsComponent } from './list-patents.component';

describe('ListPatentsComponent', () => {
  let component: ListPatentsComponent;
  let fixture: ComponentFixture<ListPatentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPatentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPatentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
