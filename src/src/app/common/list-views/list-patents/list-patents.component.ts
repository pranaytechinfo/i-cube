import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-list-patents',
  templateUrl: './list-patents.component.html',
  styleUrls: ['./list-patents.component.scss']
})
export class ListPatentsComponent implements OnInit {
  @Input() loading;
  @Input() config;
  @Input() records;
  @Input() highlightObj;

  constructor() { }

  ngOnInit() {
  }

}
