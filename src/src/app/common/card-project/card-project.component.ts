import { Component, OnInit, Input } from '@angular/core';
import { DashboardService } from './../../master/dashboard/dashboard.service';
import { ToastrService } from 'ngx-toastr';
import { General } from 'src/app/util/common/general';

@Component({
  selector: 'app-card-project',
  templateUrl: './card-project.component.html',
  styleUrls: ['./card-project.component.scss']
})
export class CardProjectComponent implements OnInit {
  @Input() swiftId;
  @Input() clientId;
  
  boolPreloader: boolean = false;
  projData: any;

  constructor(
    private _dashboardService: DashboardService,
    private _toastr: ToastrService,
    private _general: General,
  ) { }


  ngOnInit() {
    this.getProjectData();
  }


  getProjectData() {
    this.boolPreloader = true;

    this._dashboardService.getProjectData(this.swiftId.typeId, 0, 5).subscribe((res: any) => {

      this.boolPreloader = false;

      this.projData = res.result;

    }, err => {
      console.log(err);
      this.boolPreloader = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })
  }

}
