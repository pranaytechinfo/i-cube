import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderProjComponent } from './header-proj.component';

describe('HeaderProjComponent', () => {
  let component: HeaderProjComponent;
  let fixture: ComponentFixture<HeaderProjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderProjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderProjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
