import { Component, OnInit, TemplateRef } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { General } from '../../util/common/general';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DashboardService } from './../../master/dashboard/dashboard.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/auth/_helper/must-match.validator';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: any;
  userName: string;


  modalRef: BsModalRef;
  modalClientRef: BsModalRef;
  oldPassword: any = '';
  newPassword: any = '';
  confirmPassword: any = '';
  boolPreloader: boolean = false;
  errorBool: boolean = false;
  errorLengthBool: boolean = false;

  selectedProject;
  selectedClientProject;
  saveClientProjectName;

  swiftProjects: any[] = [];
  clientProjects: any[] = [];

  boolPreloaderProj: boolean = false;

  passwordIconState = {
    oldPass: true,
    newPass: true,
    confirmPass: true,

  }

  changePasswordForm: FormGroup;
  submitted = false;
  loading: boolean = false;


  constructor(
    private _toastr: ToastrService,
    private general: General,
    private _auth: AuthService,
    private _dashboardService: DashboardService,
    private modalService: BsModalService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _route: Router,
  ) { }

  ngOnInit() {


    this.changePasswordForm = this.formBuilder.group({
      newPassword: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
      oldPassword: ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validator: MustMatch('newPassword', 'confirmPassword')
    });

    this.route.params.subscribe(res => {
      this.isLoggedIn = this._auth.accessToken;
      this.userName = this._auth.user && this._auth.user.userName.split('@')[0];

      this.getCompInsightProject();
    });

  }

  goToDashboard(id) {
    this.router.navigate(['/projects/' + id + '/dashboard']);
  }

  get f() {
    return this.changePasswordForm.controls;
  }

  getCompInsightProject() {

    let savedClient = JSON.parse(sessionStorage.getItem("icubeSavedClientList"));
    let savedClientActive = JSON.parse(sessionStorage.getItem("icubeSavedClientActive"));

    if (!savedClient) {
      this.boolPreloaderProj = true;

      this._dashboardService.getCompInsightProject().subscribe((res: any) => {
        //  console.log(res);
        this.boolPreloaderProj = false;

        if (res.result && res.result[0]) {
          this.clientProjects = res.result;
          this.selectedClientProject = res.result[0].projectId;
          this.swiftProjects = res.result[0].types;
          this.selectedProject = res.result[0].projectId;
          this.saveClientProjectName = res.result[0].name;

          sessionStorage.setItem("icubeSavedClientList", JSON.stringify(res.result));
          sessionStorage.setItem("icubeSavedClientActive", JSON.stringify(res.result[0]));

        }

      }, err => {
        console.log(err);
        this.boolPreloaderProj = false;
        this._toastr.error('', this.general.getErrorMsg(err));
      })

    } else {

      this.clientProjects = savedClient;
      this.selectedClientProject = savedClientActive.projectId;
      this.selectedProject = savedClientActive.projectId;
      this.saveClientProjectName = savedClientActive.name;
      this.swiftProjects = savedClientActive.types;

    }



  }

  getTypesBasedOnProjectId(projectId) {
    const currentProject = this.clientProjects.find(i => i.projectId === projectId);
    sessionStorage.setItem("icubeSavedClientActive", JSON.stringify(currentProject));

    this.goToDashboard(currentProject.projectId);

  }

  saveClient() {
    if (!!this.selectedProject) {
      this.getTypesBasedOnProjectId(this.selectedProject);
      this.getCompInsightProject()

      this.closeClientModal();

    } else {
      this._toastr.error('Selected Project cannot be empty.')
    }


  }

  openModal(template: TemplateRef<any>) {
    this.changePasswordForm.reset()

    this.modalRef = this.modalService.show(template)
  }



  closeModal() {
    this.modalRef.hide();
    this.oldPassword = '';
    this.newPassword = '';
    this.confirmPassword = '';
  }

  openSelectProjectModal(template: TemplateRef<any>) {
    this.modalClientRef = this.modalService.show(template)
  }

  closeClientModal() {
    this.modalClientRef.hide();
  }


  handleLengthError() {
    if (this.newPassword.length < 6) {
      this.errorLengthBool = true;
    }
    else {
      this.errorLengthBool = false;
    }
  }

  handlePassMatch() {
    if (this.errorLengthBool) {
      return true;
    } else {
      return false;
    }
  }

  handleValidation() {
    if (this.newPassword !== this.confirmPassword) {
      this.errorBool = true;
    }
    else {
      this.errorBool = false;
    }
  }

  resetPassword() {
    this.loading = true
    this.boolPreloader = true;

  console.log(this.changePasswordForm.value)
    this._auth.resetPassword(this.changePasswordForm.value).subscribe((res: any) => {

      this.boolPreloader = false;
      this._toastr.success("Password successfully updated, please login again with new credentials");
      this.loading = false
      this.modalRef.hide();

      this.router.navigateByUrl('/auth/logout')

    }, err => {
      this.boolPreloader = false;
      this.loading = false

      this._toastr.error('', this.general.getErrorMsg(err));
    })
  }


  hideShowEye(type) {
    this.passwordIconState[type] = !this.passwordIconState[type]

  }

}
