import { Component, OnInit, ViewChild, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  @ViewChild('myTable', { static: false }) table: any;
  @Input() loading;
  @Input() config;
  @Input() records;
  @Input() total;
  @Input() highlightObj;


  @Output() sendSort = new EventEmitter<any>();
  @Output() toggleClickedVal = new EventEmitter<any>();

  selectedCells: any = [];




  constructor() { }

  ngOnInit(): void {
  }


  toggleExpandRow(row) {
    //console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onSort(event) {
    // event was triggered, start sort sequence

    if (event.sorts[0]) {
      var sortKey = event.sorts[0].prop + " " + event.sorts[0].dir;
      this.sendSort.emit(sortKey)
    }


  }

  onSelect({ selected }) {
    this.selectedCells.splice(0, this.selectedCells.length);
    this.selectedCells.push(...selected);
    this.toggleClickedVal.emit({ data: this.selectedCells })

  }

  checkUncheckallRecords(event) {
    let isAllRowSelected = this.isSelectedAll()
    this.records = this.records.map((record) => { return { ...record, checked: !isAllRowSelected } })
     if (event.currentTarget.checked) {

      if (this.selectedCells.length == 0) {
        this.records.forEach((recdata, rindex) => { this.selectedCells.push(recdata) })
      } else {
        this.records.forEach((recdata, rindex) => {
          const rowindex = this.getRowIndex(this.selectedCells, recdata.mcId)
          if (rowindex < 0) { this.selectedCells.push(recdata) }

        })
      }


    } else {
      this.records.forEach((rdata, rindex) => {
        const rowindex = this.getRowIndex(this.selectedCells, rdata.mcId)
        this.selectedCells.splice(rowindex, 1)
      })
    }

    this.toggleClickedVal.emit({ data: this.selectedCells })

  }

  checkIndividualRecord(rowData, event) {
    const recrowindex = this.getRowIndex(this.records, rowData.mcId)
    if (event.currentTarget.checked) {
      this.records[recrowindex].checked = true
      this.selectedCells.push(rowData)

    } else {
      this.records[recrowindex].checked = false
      const rowindex = this.getRowIndex(this.selectedCells, rowData.mcId)
      this.selectedCells.splice(rowindex, 1)

    }
    this.toggleClickedVal.emit({ data: this.selectedCells })

  }


  isSelectedAll() {
   let bool = true;

    this.records.forEach((item: any) => {
      if (!item.checked) {
        bool = false;
      }
    })

    return bool;
  }


  getRowIndex(searchArray: any[], mcID) {
    const isLargeNumber = (element) => element.mcId === mcID;
    const index = searchArray.findIndex(isLargeNumber);
    return index
  }

  getSelectedCount(){
  const selecteddRows = this.records.filter((rec)=>{return rec.checked===true})
   return  selecteddRows.length

  }

clearSelcetd(){
  this.selectedCells=[];
  this.toggleClickedVal.emit({ data: this.selectedCells });
  this.records = this.records.map((record) => { return { ...record, checked: false} });
  this.getSelectedCount();
}

}
