import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalFacetsComponent } from './global-facets.component';

describe('GlobalFacetsComponent', () => {
  let component: GlobalFacetsComponent;
  let fixture: ComponentFixture<GlobalFacetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalFacetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalFacetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
