import { Component, Input, OnInit, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-global-facets',
  templateUrl: './global-facets.component.html',
  styleUrls: ['./global-facets.component.scss']
})
export class GlobalFacetsComponent implements OnInit, OnChanges {
  @Input() inputFacets;
  @Input() resetBool;

  @Output() sendSelFacets = new EventEmitter<object>();
  @Output() sendClickReset = new EventEmitter<object>();

  selectedFacets: any = {};
  added: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'resetBool': {
            
            this.selectedFacets = {};
        
            this.added = false;

          }
        }
      }
    }

  }

  onChange(data, value) {
    data.isChecked = value;


    this.inputFacets.forEach(blocks => {
      let checkedArr = blocks.value.filter((ea) => {
        return ea.isChecked
      })

      if (checkedArr.length >= 1) {
        this.selectedFacets[blocks.facetKey] = checkedArr.map(function (item) { return item['key']; });;
      } else {
        delete this.selectedFacets[blocks.facetKey];
      }


    });


  }


  onApplyFilter() {


    this.sendSelFacets.emit({facets: this.selectedFacets});
    this.added = true;
  }

  onReset() {

    this.selectedFacets = {};

    this.added = false;

    this.sendClickReset.emit(this.selectedFacets);

  }

}
