import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-primary-facets',
  templateUrl: './primary-facets.component.html',
  styleUrls: ['./primary-facets.component.scss']
})
export class PrimaryFacetsComponent implements OnInit, OnChanges {
  @Input() inputFacets: any;
  @Input() resetBool: any;

  @Output() sendPrimFacets = new EventEmitter<any>();

  model: string = "";

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'resetBool': {
            this.model = '';
          }
        }
      }
    }

  }

  handleChange(eve) {
    this.sendPrimFacets.emit(this.model)
  }

}
