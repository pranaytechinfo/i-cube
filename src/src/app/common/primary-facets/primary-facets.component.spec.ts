import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryFacetsComponent } from './primary-facets.component';

describe('PrimaryFacetsComponent', () => {
  let component: PrimaryFacetsComponent;
  let fixture: ComponentFixture<PrimaryFacetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryFacetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryFacetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
