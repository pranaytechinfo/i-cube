import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GlobalSearchService {

  constructor(
    public http: HttpClient
  ) { }


  gloablSearchApi(ids, start, limit, selFactes, query, graphClick, swiftTypes, graphConfig) {

    let urlOptions = "?start=" + start + "&limit=" + limit;

    let body = {
      typeIds: ids,
      query: query,
      facets: selFactes,
      graphFilter: graphClick,
      types: swiftTypes,
      graphConfig:graphConfig
    }

    return this.http.post(environment.app.ouathURL + 'api/v1/project/globalSearch' + urlOptions, body);
  }

  getGlobalHeaders(cId) {
    return this.http.get(environment.app.ouathURL + 'api/v1/project/globalSearchHeaders/' + cId);
  }

  getGlobalAutosuggest(cId, field) {
    return this.http.get(environment.app.ouathURL + 'api/v1/project/keywords/' + cId + "?fieldName=" + field);
  }

  getgloablGraphApi(ids, config, selFactes, query) {

    let body = {
      "config": config,
      "query": query,
      "facets": selFactes,
    }

    return this.http.post(environment.app.ouathURL + 'api/v1/project/globalSearchGraphData/' + ids, body);
  }


  getExcelViewService(Ids,query, selFactes, swiftTypes,  graphClick, graphConfig) {
    let body = {
      typeIds:Ids,
      query: query,
      facets: selFactes,
      graphFilter: graphClick,
      types: swiftTypes,
      graphConfig:graphConfig,
      projectName:JSON.parse( sessionStorage.getItem('icubeSavedClientActive')).name

    }
    return this.http.post(environment.app.ouathURL + 'api/v1/project/globalSearchExport?sort=releaseDate desc', body );
  }
}
