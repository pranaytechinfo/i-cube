import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { General } from '../../util/common/general';
import { DashboardService } from './../dashboard/dashboard.service';
import { GlobalSearchService } from './global-search.service';
import { GlobalGraphBuilderComponent } from './../../common/global-graph-builder/global-graph-builder.component';
import { environment } from 'src/environments/environment.prod';


@Component({
  selector: 'app-global-search',
  templateUrl: './global-search.component.html',
  styleUrls: ['./global-search.component.scss']
})
export class GlobalSearchComponent implements OnInit {
  @ViewChild('globalGraphBuilder', {static: false}) graphBuildercomp: GlobalGraphBuilderComponent = {} as GlobalGraphBuilderComponent;


  viewTable: string = "table";

  swiftProjectsIds: any[] = [];
  swiftProjects: any[] = [];

  boolPreloaderProj: boolean = false;
  exportStatus:boolean = environment.app.barActionButtonStatus


  dataRecords: any[] = [];
  total: number = 0;
  dataFacets: any[] = [];
  boolPreloader: boolean = false;

  //For higlight
  highlightObj: object = {};


  //Pagination sort factes params
  start: number = 0;
  pageOffset: number = 10;
  totalCount: number = 0;
  facets: object = {};
  basicSearch: string = '';
  graphClickFilter: object = {};

  //Reset
  filterResetBool = false;
  mainResetBool = false;
  paginateResetBool = false;
  currentProjectId: string;

  showGraph: boolean = false;
  graphData: any = {};
  graphConfig: any = {};
  boolPreloaderGraph: boolean = false;

  //DataExport
  boolPreloaderExport = false;


  constructor(
    private _toastr: ToastrService,
    private _general: General,
    private _dashboardService: DashboardService,
    private _globalService: GlobalSearchService,
    private route: ActivatedRoute

  ) { }

  ngOnInit() {
    this.currentProjectId = this.route.snapshot.params['clientId']
    this.getCompInsightProject();
  }

  getCompInsightProject() {
    this.boolPreloader = true;
    var THIS = this;

    this._dashboardService.getCompInsightProject().subscribe((res: any) => {
      if (res.result && res.result[0]) {
        const currentProjData = res['result'].filter(book => book['projectId'] === THIS.currentProjectId);
        this.swiftProjects = currentProjData[0].types;
        let swiftProjects = currentProjData[0].types;

        let arr = [];
        swiftProjects.forEach(element => {
          arr.push(element.typeId);
        });

        this.swiftProjectsIds = arr;

        this.globalSearch();

      }

    }, err => {
      console.log(err);
      this.boolPreloader = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })
  }

  onFacetsSelcted($event) {
    this.clearPagination();

    this.facets = $event.facets;

    this.callTableGraphApi();
  }

  callTableGraphApi() {
    if (this.showGraph) {
      this.getGraphApi()
    }

    this.globalSearch()
  }

  onPaginate($event) {
    this.start = $event.start;
    this.pageOffset = $event.offset;

    this.globalSearch();
  }

  onBasicSearch(term) {
    this.clearPagination();

    this.basicSearch = term;
    this.callTableGraphApi();
  }

  onFilterReset() {

    this.clearPagination();

    this.facets = {};

    this.filterResetBool = !this.filterResetBool;

    this.callTableGraphApi();
  }

  onMainReset() {
    this.clearPagination();

    this.facets = {};

    this.graphConfig={}
    this.basicSearch = "";

    this.filterResetBool = !this.filterResetBool;
    this.mainResetBool = !this.mainResetBool;

    //Graph clear
    this.graphBuildercomp.graphForm.reset()
    this.graphData = {};
    this.graphClickFilter = {};

    this.globalSearch()
  }

  clearPagination() {
    this.start = 0;
    this.paginateResetBool = !this.paginateResetBool;
  }

  onGraphClick(data) {
    this.graphClickFilter = data;

    let el = document.getElementById("borderGraphTable");
    el.scrollIntoView({behavior:"smooth"});

    this.globalSearch();

  }



  graphClear(data) {
    this.graphClickFilter = {};
    this.graphData = {};
   this.graphConfig={}
    this.globalSearch();
  }

  resetGraphFilters() {
    this.graphClickFilter = {};

    this.globalSearch();
  }

  onGraphSubmit(data) {
    this.graphConfig = data;

    this.getGraphApi();
    this.globalSearch();
  }

  getGraphApi() {

    this.boolPreloaderGraph = true;

    this._globalService.getgloablGraphApi(this.currentProjectId, this.graphConfig, this.facets, this.basicSearch).subscribe((res: any) => {
       this.boolPreloaderGraph = false;

      this.graphData = res.result;



    }, err => {
      console.log(err);
      this.boolPreloaderGraph = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })

  }

  globalSearch() {

    this.boolPreloader = true;
    this.dataRecords = [];

    this.highlightObj = {
      facets: this.facets,
      query: this.basicSearch
    }

    this._globalService.gloablSearchApi(this.swiftProjectsIds, this.start, this.pageOffset, this.facets, this.basicSearch, this.graphClickFilter, this.swiftProjects, this.graphConfig).subscribe((res: any) => {
       this.boolPreloader = false;

      this.dataRecords = res.result.records;

      this.total = res.result.total;
      this.dataFacets = res.result.facets;
    }, err => {
      console.log(err);
      this.boolPreloader = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })
  }



  onGlobalDataExportAPI() {
    this.boolPreloaderExport = true;
    this._globalService.getExcelViewService(this.swiftProjectsIds, this.basicSearch,  this.facets,this.swiftProjects,  this.graphClickFilter, this.graphConfig)
    .subscribe((res: any) => {
      // var downloadURL = window.URL.createObjectURL(res);
      // var link = document.createElement('a');
      // link.href = downloadURL;
      // link.download = "3i_Global_Search_exported.xlsx";
      // link.click();
      this._toastr.success(res.result);
      this.boolPreloaderExport = false;
    }, (err: any) => {
      console.log(err);
      this.boolPreloaderExport = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    });
  }


}





