import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { General } from 'src/app/util/common/general';
import { DashboardService } from '../../dashboard/dashboard.service';

@Component({
  selector: 'app-executive-summary',
  templateUrl: './executive-summary.component.html'
})
export class ExecutiveSummaryComponent implements OnInit {
  data: any;
  clientId: any;
  chartData: any;

  dataRecords: any[] = [];
  boolPreloader: boolean = false;

  dataFacets: any[] = [];
  facets: object = {};

  //Reset
  filterResetBool = false;


  constructor(private dashboardService: DashboardService, private _general: General,
    private _toastr: ToastrService, private _route: ActivatedRoute) {

  }

  ngOnInit(): void {

    this._route.params.subscribe(res => {
      this.clientId = res['clientId'];

      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });

      this.getGraphData();
      this.getProjectData()
    });
  }

  getGraphData() {
    const body = {
      facets: this.facets
    }
    this.boolPreloader = true;

    this.dashboardService.getExecutiveSummaryDetail(this.clientId, body).subscribe(res => {
      if (!!res) {
        this.chartData = !!res['result'] ? res['result'] : [];
        this.initChart();
      }
      this.boolPreloader = false;

    }, err => {
      console.log(err);
      this.boolPreloader = false;

      this._toastr.error('', this._general.getErrorMsg(err));
    })
  }

  initChart() {

    this.data = {
      chart: {
        type: 'variablepie'
      },
      title: '',
      tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
          'Total Count: <b>{point.y}</b><br/>' +
          ''
      },
      series: [{
        minPointSize: 100,
        innerSize: '30%',
        zMin: 0,
        name: 'Data',
        dataLabels: {
          formatter: function () {
            return this.point.name + " (" + this.y + ")";
          },
        },
        data: this.chartData
      }],
      "plotOptions": {
        "pie": {
          "dataLabels": {
            enabled: true,
            y: -25,
            verticalAlign: 'top'
          }
        },

      },
    }


  }

  onFacetsSelcted($event) {
    this.facets = $event.facets;

    this.getProjectData();
    this.getGraphData();

  }



  onFilterReset() {
    this.facets = {};
    this.filterResetBool = !this.filterResetBool;

    this.getProjectData();
    this.getGraphData();

  }

  getProjectData() {

    this.dataRecords = [];

    this.dashboardService.getExecutiveSummaryData(this.clientId, this.facets).subscribe((res: any) => {

      this.dataRecords = res.result.records;

      if(this.facets && Object.keys(this.facets).length === 0 ) {
        this.dataFacets = res.result.facets;
      } else {
        res.result.facets.forEach(block => {

          if(this.facets[block.facetKey] && this.facets[block.facetKey].length >= 1) {
            block.value.forEach(each => {
              if(this.facets[block.facetKey].includes(each.key)) {
                each.isChecked = true;
              }
            })

          }
         
        });

        this.dataFacets = res.result.facets;
      }

    }, err => {
      console.log(err);
      this._toastr.error('', this._general.getErrorMsg(err));
    })
  }
}
