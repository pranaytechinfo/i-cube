import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    public http: HttpClient
  ) { }


  getCompInsightProject() {
    return this.http.get(environment.app.ouathURL + 'api/v1/project/clientProjects');
  }

  getExecutiveSummaryDetail(projectId, body) {
    return this.http.post(environment.app.ouathURL + 'api/v1/project/executiveSummary/' + projectId, body);
  }

  getAnalyticsGraphConfig(projectId) {
    // projectId = 'C1';
    return this.http.get(environment.app.ouathURL + 'api/v1/project/graphConfig/' + projectId);
  }

  getAnalyticsGraphData(projectId, typeId, body) {
    // projectId = 'C1';
    return this.http.post(environment.app.ouathURL + 'api/v1/project/analytics/' + projectId + '/' + typeId, body);
  }


  getProjectData(id, start, limit) {

    return this.http.post(environment.app.ouathURL + 'api/v1/project/search/' + id + "?sort=releaseDate desc&start=" + start + "&limit=" + limit, "");
  }

  dataSearchApi(id, start, limit, sort, selFactes, primFacet, dateFilters, query, graphClick) {

    let urlOptions = "?sort=" + sort + "&start=" + start + "&limit=" + limit;

    let body = {
      query: query,
      facets: selFactes,
      filter: primFacet,
      dateFilter: dateFilters,
      graphFilter: graphClick
    }

    return this.http.post(environment.app.ouathURL + 'api/v1/project/search/' + id + urlOptions, body);
  }

  getExecutiveSummaryData(clientId, facetsData) {
    let body = {
      facets: facetsData

    }

    return this.http.post(environment.app.ouathURL + 'api/v1/project/executiveSummaryDetails/' + clientId , body);
  }

  getExcelViewService(id: string, selFactes, primFacet, dateFilters, query, graphClick) {
    let body = {
      query: query,
      facets: selFactes,
      filter: primFacet,
      dateFilter: dateFilters,
      graphFilter: graphClick,
      projectName:JSON.parse( sessionStorage.getItem('icubeSavedClientActive')).name
    }
  //  return this.http.post(environment.app.ouathURL + 'api/v1/project/export/' + id +'?sort=releaseDate desc', body, { responseType: 'blob' });
    return this.http.post(environment.app.ouathURL + 'api/v1/project/export/' + id +'?sort=releaseDate desc', body);

}

  graphConfig(tid) {

    return this.http.get(environment.app.ouathURL + 'api/v1/project/graphTypes/' + tid);
  }

  graphData(tid, config, selFactes, primFacet, dateFilters, query) {

    const body = {
      config: config,
      query: query,
      facets: selFactes,
      filter: primFacet,
      dateFilter: dateFilters
    }

    return this.http.post(environment.app.ouathURL + 'api/v1/project/graphData/' + tid, body);
  }


}
