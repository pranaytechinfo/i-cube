import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { ToastrService } from 'ngx-toastr';
import { General } from 'src/app/util/common/general';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  boolPreloader: boolean = false;
  swiftProjects: any[] = [];
  currentProjectId: any;

  constructor(
    private _dashboardService: DashboardService,
    private _toastr: ToastrService,
    private _general: General,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });


    this.route.params.subscribe(res => {
      this.currentProjectId = res['clientId']

      this.getCompInsightProject();

    });

  }


  getCompInsightProject() {
    this.boolPreloader = true;
    var THIS = this;

    this._dashboardService.getCompInsightProject().subscribe((res: any) => {

      this.boolPreloader = false;

      if (res.result && res.result[0]) {
        const currentProjData = res['result'].filter(book => book['projectId'] === THIS.currentProjectId);
        this.swiftProjects = currentProjData[0].types;
      }

    }, err => {
      console.log(err);
      this.boolPreloader = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })
  }



  getInvalidProjects(proj) {
    if (proj['name'].includes('Executive Summary')) {
      return false
    } else {
      return true;
    }
  }


}
