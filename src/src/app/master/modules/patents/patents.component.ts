import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from './../../dashboard/dashboard.service';
import { ToastrService } from 'ngx-toastr';
import { General } from 'src/app/util/common/general';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';
import { DataTableComponent } from 'src/app/common/data-table/data-table.component';

@Component({
  selector: 'app-patents',
  templateUrl: './patents.component.html',
  styleUrls: ['./patents.component.scss']
})
export class PatentsComponent implements OnInit {
  @ViewChild('DataTable',{static: false}) DataTable: DataTableComponent = {} as DataTableComponent;

  pId: any;
  boolPreloader: boolean = false;
  viewTable: string = "table";

  dataRecords: any[] = [];

  marked;
  tableMap = new Map();


  total: number = 0;
  config: any[] = [];
  dataFacets: any[] = [];
  dataPrimFacets: any[] = [];
  typeData: string = "";

  selectedrowData: any[] = []
  exportSelectedbuttonStatus:Boolean=false


  //Pagination sort factes params
  start: number = 0;
  pageOffset: number = 10;
  totalCount: number = 0;
  sort: string = 'releaseDate desc';
  facets: object = {};
  primaryFacet: string = '';
  dateFilter: object = {};
  basicSearch: string = '';
  graphClickFilter: object = {};

  //Reset
  filterResetBool = false;
  mainResetBool = false;
  paginateResetBool = false;

  //For higlight
  highlightObj: object = {};
  boolPreloaderExport = false;

  //For Graph Logics
  graphConfigsData: any[] = [];
  boolPreloaderConfigs: boolean = false;

  graphData: any[] = [];
  boolPreloaderGraph: boolean = false;
  showGraph: boolean = false;
  isGraphAvailable: boolean = false;
  exportStatus: boolean = environment.app.barActionButtonStatus
  constructor(
    private _dashboardService: DashboardService,
    private _toastr: ToastrService,
    private _general: General,
    private _route: ActivatedRoute,
    private _router: Router,


  ) {
    this._router.routeReuseStrategy.shouldReuseRoute = () => false;

  }

  ngOnInit() {


    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });

    this._route.params.subscribe(res => {
      this.pId = res['id'];

      this.getProjectData();
      this.getGraphConfigsApi();
    });

  }

  onFacetsSelcted($event) {
    this.clearPagination();

    this.facets = $event.facets;
    this.dateFilter = $event.dateFacets;

    this.getProjectData();
    this.getGraphDataAll();
  }

  onPrimaryFacets($event) {
    this.clearPagination();

    this.primaryFacet = $event;

    this.getProjectData();
    this.getGraphDataAll();
  }


  onPaginate($event) {
    this.start = $event.start;
    this.pageOffset = $event.offset;


    this.getProjectData();
  }

  onSort($event) {
    this.clearPagination();

    this.sort = $event;

    this.getProjectData();
  }

  onBasicSearch(term) {
    this.clearPagination();

    this.basicSearch = term;
    this.getProjectData();
    this.getGraphDataAll();
  }

  onFilterReset() {

    this.clearPagination();

    this.facets = {};
    this.dateFilter = {};
    this.primaryFacet = "";

    this.filterResetBool = !this.filterResetBool;

    this.getProjectData();
    this.getGraphDataAll();
  }

  onMainReset() {
    this.clearPagination();
    this.clearAllObj();

    this.getProjectData();
    this.getGraphDataAll();
  }

  onGraphClick(data) {
    this.graphClickFilter = data;

    let el = document.getElementById("borderGraphTable");
    el.scrollIntoView({ behavior: "smooth" });

    this.getProjectData();
  }

  resetGraphFilters() {
    this.graphClickFilter = {};

    this.getProjectData();
  }

  clearAllObj() {
    this.facets = {};
    this.dateFilter = {};
    this.graphClickFilter = {};
    this.primaryFacet = "";
    this.basicSearch = "";

    this.filterResetBool = !this.filterResetBool;
    this.mainResetBool = !this.mainResetBool;
  }

  clearPagination() {
    this.start = 0;
    this.paginateResetBool = !this.paginateResetBool;
  }


  getProjectData() {

    this.boolPreloader = true;
    this.dataRecords = [];

    this.highlightObj = {
      facets: this.facets,
      query: this.basicSearch
    }

    this._dashboardService.dataSearchApi(this.pId, this.start, this.pageOffset, this.sort, this.facets, this.primaryFacet, this.dateFilter, this.basicSearch, this.graphClickFilter).subscribe((res: any) => {

      this.boolPreloader = false;

      this.config = res.result.config.headers;
      this.dataRecords = res.result.records;
      this.total = res.result.total;
      this.dataPrimFacets = res.result.primaryFacets;
      this.typeData = res.result.config.type;
      this.dataFacets = res.result.facets;
      let selectedCount = 0;
        this.dataRecords.map((datares) => {
        if (this.tableMap.get(datares.mcId)) {
          datares['checked'] = true;
          selectedCount++;
        } else {
          datares['checked'] = false;
        }

        this.marked = (selectedCount == this.dataRecords.length) ? true : false;
        this.boolPreloader = false;
      });





    }, err => {
      console.log(err);
      this.boolPreloader = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })
  }

  onExportAPI() {
    this.boolPreloaderExport = true;
    let selectedMCIDs = Array.from( this.tableMap.keys())
    if(selectedMCIDs.length>0){
        this.facets["mcId"] = selectedMCIDs
    }


    // this._dashboardService.getExcelViewService(this.pId, this.facets, this.primaryFacet, this.dateFilter, this.basicSearch, this.graphClickFilter).subscribe((res: any) => {
    //   var downloadURL = window.URL.createObjectURL(res);
    //   var link = document.createElement('a');
    //   link.href = downloadURL;
    //   link.download = "3i_exported.xlsx";
    //   link.click();
    //   this._toastr.success('Data Exported!');
    //   this.boolPreloaderExport = false;
    // }, (err: any) => {
    //   console.log(err);
    //   this.boolPreloaderExport = false;
    //   this._toastr.error('', this._general.getErrorMsg(err));
    // });


    this._dashboardService.getExcelViewService(this.pId, this.facets, this.primaryFacet, this.dateFilter, this.basicSearch, this.graphClickFilter).subscribe((res: any) => {
      // var downloadURL = window.URL.createObjectURL(res);
      // var link = document.createElement('a');
      // link.href = downloadURL;
      // link.download = "3i_exported.xlsx";
      // link.click();
     delete this.facets["mcId"];
      this._toastr.success(res.result);
      this.boolPreloaderExport = false;
      this.DataTable.clearSelcetd();
    }, (err: any) => {
      console.log(err);
      this.boolPreloaderExport = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    });
  }

  getGraphConfigsApi() {

    this.boolPreloaderConfigs = true;
    this.graphConfigsData = [];

    this._dashboardService.graphConfig(this.pId).subscribe((res: any) => {

      this.boolPreloaderConfigs = false;
      this.graphConfigsData = res.result;

      if (this.graphConfigsData.length >= 1) {

        this.isGraphAvailable = true;
        this.getGraphDataAll();
      } else {
        this.isGraphAvailable = false;
      }

    }, err => {
      console.log(err);
      this.boolPreloaderConfigs = false;
      this.isGraphAvailable = false;
      this._toastr.error('', this._general.getErrorMsg(err));
    })
  }



  getGraphDataAll() {


    this.boolPreloaderGraph = true;

    const reqs = [];
    this.graphConfigsData.forEach(doc => {

      reqs.push(this._dashboardService.graphData(this.pId, doc, this.facets, this.primaryFacet, this.dateFilter, this.basicSearch)
        .pipe(catchError(value => of({ failed: true })))

      );
    }
    );

    forkJoin(reqs).subscribe(resAll => {
      const filteredArr = resAll.filter((each: any) => {
        if (!each.failed) {
          return each;
        } else {
          this._toastr.error('Error!', ' encountered a error! ');
          return;
        }
      });


      filteredArr[0]['active'] = true;

      this.graphData = filteredArr;
      this.boolPreloaderGraph = false;

    }, err => {
      this.boolPreloaderGraph = false;
      console.log(err);
      this._toastr.error('Error!', 'Please try again after some time!');
    });

  }


  getTableSelectedRows(selectedrowDatain) {
//    this.selectedrowData = selectedrowDatain.data
     this.tableMap.clear();
     selectedrowDatain.data.forEach((data, index) => {
      this.tableMap.set(String(data.mcId), true);
    })
 }


}
