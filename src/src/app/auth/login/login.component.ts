import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DashboardService } from 'src/app/master/dashboard/dashboard.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  errMsg: string = '';


  constructor(
    private _route: Router,
    private formBuilder: FormBuilder,
    private _authService: AuthService,
    private _dashboardService: DashboardService
  ) {
    sessionStorage.clear();
  }

  get f() {
    return this.loginForm.controls;
  }

  ngOnInit() {
    this.submitted = false;
    this.loading = false;

    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(4)]],
    });
  }

  onAuthSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }


    this.loading = true;
    sessionStorage.clear();

    this._authService.onLogin(this.loginForm.value).subscribe((res: any) => {
      this.getCompInsightProject();

    }, err => {
      console.log(err)
      this.loading = false;
      this.errMsg = err.error.error_description ? err.error.error_description : err.error.error;
    });


  }

  getCompInsightProject() {

    this._dashboardService.getCompInsightProject().subscribe((res: any) => {

      if(res.result && res.result[0]) {
        this._route.navigate(['/projects/' + res.result[0].projectId + '/dashboard']);
      }
      this.loading = false;

    }, err => {
      console.log(err);
      this.loading = false;

      // this._toastr.error('', this._general.getErrorMsg(err));
    })
  }

  

  hideErr() {
    this.errMsg = '';
  }
}
