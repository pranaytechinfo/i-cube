import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from "../auth.service";
import { ToastrService } from "ngx-toastr";
import { General } from '../../util/common/general';
import { MustMatch } from 'src/app/auth/_helper/must-match.validator';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  submitted = false;
  identifier: string;
  loading: boolean = false;
  boolShowForm: boolean = true;
  boolShowForm1: boolean = true;
  boolpreDetails: boolean = true;
  errorBool: boolean = false;

  passwordIconState ={

    newPass : true,
    confirmPass : true,
}


  constructor(private formBuilder: FormBuilder,
    private general: General,
    private _auth: AuthService,
    private activeRoute: ActivatedRoute,
    private toastr: ToastrService) {
    this.activeRoute.queryParams.subscribe(params => {
      this.identifier = params['token'];
      this.reset();
    });
  }

  ngOnInit() {

      this.changePasswordForm = this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
      },{
        validator: MustMatch('password', 'confirmPassword')
      });

  }

  get f() { return this.changePasswordForm.controls; }



  reset() {
    this.boolpreDetails = true;

    // this.boolShowForm= false;
    // this.boolpreDetails = false;

    this._auth.verifyToken(this.identifier).subscribe(res => {
      this.boolShowForm = res['message'];
      this.boolpreDetails = false;
    }, err => {
      // console.log(err)
      this.toastr.error('', this.general.getErrorMsg(err));
      this.boolpreDetails = false;

    });
  }

  onSubmit() {
    this.submitted = true;


    if (this.changePasswordForm.invalid || this.errorBool) {
      return;
    }
    this.loading = true;
    this._auth.onResetPassword(this.identifier,this.changePasswordForm.value).subscribe(res => {
      this.boolShowForm1 = res['message'] ;
      this.loading = false;
      this.toastr.success('Success');

    }, err => {
      this.loading = false;
    this.toastr.error('', this.general.getErrorMsg(err));
    });

  }

  hideShowEye(type){
    this.passwordIconState[type] = !this.passwordIconState[type]

  }


}
