import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { tap } from 'rxjs/internal/operators';
import { BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public loaderSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private popupSubject = new BehaviorSubject<any>({});
  popup = this.popupSubject.asObservable();

  public _preLoader: boolean;
  public _user;
  public _accessToken;
  public _refreshToken;


  setPopupSubject(val) {
    this.popupSubject.next(val)
  }

  getPopupSubject() {
    return this.popupSubject;
  }

  get preLoader() {
    return this._preLoader;
  }

  set preLoader(value) {
    if (value) {
      this.loaderSubject.next(this.loaderSubject.value + 1);
    } else {
      this.loaderSubject.next(this.loaderSubject.value - 1);
    }
    this._preLoader = value;
  }

  constructor(public http: HttpClient) {
    this.doLoginUser = this.doLoginUser.bind(this);
  }

  get user() {
    this._user = JSON.parse(sessionStorage.getItem('user'));
    return this._user;
  }

  set user(value) {
    sessionStorage.setItem('user', JSON.stringify(value));
    this._user = value;
  }

  get accessToken() {
    this._accessToken = sessionStorage.getItem('access_token');
    return this._accessToken;
  }

  set accessToken(value) {
    sessionStorage.setItem('access_token', value);
    this._accessToken = value;
  }

  get refreshToken() {
    this._refreshToken = sessionStorage.getItem('refresh_token');
    return this._refreshToken;
  }

  set refreshToken(value) {
    sessionStorage.setItem('refresh_token', value);
    this._refreshToken = value;
  }





  onLogin(objData) {

    if (objData.username && objData.password) {

      const options = {
        "userName": objData.username,
        "password": objData.password,
        "grantType": "password"
      };

      return this.http.post(environment.app.ouathURL + 'api/v1/oauth/getToken', options)
        .pipe(
          tap(tokens => this.doLoginUser(tokens, objData))
        );

    }
  }

  onRefreshToken() {

    // Refresh token login on 400 error


    if (this.accessToken && this.refreshToken) {


      const options = {
        'refresh_token': this.refreshToken,
        'access_token': this.accessToken,
        'grant_type': 'refresh_token'
      };

      return this.http.post(environment.app.ouathURL + 'api/v1/oauth/getAccessToken', options)
        .pipe(
          tap(tokens => this.doLoginUser(tokens)),
        );
    }
  }

  onLogout() {

    const options = {
      'access_token': this.accessToken
    };



    return this.http.post(environment.app.ouathURL + 'api/v1/oauth/getLogout', options)
      .pipe(
        tap(res => { this.doLogoutUser(res) })
      );
  }

  resetPassword(getData) {
    return this.http.post(environment.app.ouathURL + 'api/v1/oauth/changePassword', getData)
  }

  forgotPassword(email) {
    const body = {
      "email": email
    };

    return this.http.post(environment.app.ouathURL + 'api/v1/oauth/forgetPassword', body);
  }


  verifyToken(token) {
    return this.http.get(environment.app.ouathURL + 'api/v1/oauth/registrationConfirm?token=' + token)
  }

  onResetPassword(token, objData) {
    const data = {
      'token': token,
      'newPassword': objData.password,
      'confirmPassword': objData.confirmPassword
    }

    return this.http.post(environment.app.ouathURL + 'api/v1/oauth/resetPassword', data);

  }





  doLoginUser(tokens, formData?) {

    // Setting values in sessionStorage

    this.user = tokens;

    if (tokens['access_token']) {
      this.accessToken = tokens['access_token'];
    }


    if (tokens['refresh_token']) {
      this.refreshToken = tokens['refresh_token'];

    }
  }

  doLogoutUser(user) {
    this.user = null;
    this.accessToken = null;
    this.refreshToken = null;
  }


}
