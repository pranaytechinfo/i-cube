import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from "../auth.service";
import { ToastrService } from "ngx-toastr";
import { General } from '../../util/common/general';

@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.scss']
})
export class ForgetComponent implements OnInit {
  title: string;
  resetForm: FormGroup;
  submitted: boolean;
  loading: boolean;

  constructor(
    private general: General,
    private _auth: AuthService,
    private toastr: ToastrService,
    private _formBuilder: FormBuilder) {
    this.title = environment.app.brand.name;
  }

  get f() {
    return this.resetForm.controls;
  }

  ngOnInit() {
    this.submitted = false;
    this.loading = false;

    this.resetForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }



  onAuthSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.resetForm.invalid) {
      return;
    }

    this.loading = true;
    this._auth.forgotPassword(this.resetForm.value.email).subscribe(res => {

      this.loading = false;
      this.toastr.success("Forgot password link is successfully sent to registered Email address");
      // this.boolSentConfirm = true;
    }, err => {
      this.loading = false;
      //  console.log(err)
      this.toastr.error('', this.general.getErrorMsg(err));

    })

  }

}
