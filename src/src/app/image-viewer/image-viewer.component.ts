import { Component, Input, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-image-viewer',
  templateUrl: './image-viewer.component.html',
  styleUrls: ['./image-viewer.component.scss']
})
export class ImageViewerComponent implements OnInit {

  @Input() imageURLs;
  @Input() publicationNumber;
  @Input() title;




  current: number = 0;

  modalRef: BsModalRef;
  constructor(private modalService: BsModalService,) {

   }

  ngOnInit() {
  }

  viewImage(template) {
    this.current=0
    const config = {
      ignoreBackdropClick: false,
      class: 'modal-md modal-dialog-center',


    };

    this.modalRef = this.modalService.show(template, config);

  }

  next() {
    if ((this.current + 1) < this.imageURLs.length) {
      this.current = this.current + 1
    }
  }

  prev() {
    if (this.current != 0) {
      this.current--;
    }
  }


}
