import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlightText'
})
export class HighlightTextPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {

    var searchParam = args[0];

    var terms = [];

    //basic search
    if (searchParam.query) {
      terms.push(searchParam.query)
    }


    //Facets
    Object.keys(searchParam.facets).forEach(function (key) {

      var value = searchParam.facets[key];

      value.forEach(element => {
        terms.push(element);
      });

    });



    if (value && terms.length >= 1) {
      var re = new RegExp(terms.join("|"), "gi");

      if (typeof value === 'string') {

        return value.replace(re, (x) => "<mark class='mcMark' >" + x + "</mark>");

      } else if (typeof value === 'object') {


        let arr = [];
        value.forEach(element => {
          arr.push(element.replace(re, (x) => "<mark class='mcMark' >" + x + "</mark>"));
        });

        return arr.join(", ");
      } else {
        return value;
      }


    } else {
      if (typeof value === 'object') {
        return value.join(", ");
      } else {
        return value;
      }
    }

  }

}
