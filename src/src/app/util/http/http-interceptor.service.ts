import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, EMPTY, Observable, throwError } from 'rxjs';
import { take, filter, catchError, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { tap } from 'rxjs/internal/operators';


@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {
  refreshTokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  isRefreshing = false;

  isErrorModel = false;
  errorModelSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private _router: Router,
    public _authService: AuthService) {

  }

  addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    return req.clone({
      setHeaders: {
        'token': token
      }
    });
  }

  addToken_ForgotPassword(req: HttpRequest<any>): HttpRequest<any> {
    return req.clone({
      setHeaders: {
        
        //'x-alt-origin': window.location.origin + "/#"
        'x-alt-origin' : window.location.origin + "/competitive-intelligence/#"

      }
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (this._authService.accessToken 
      && !req.url.includes('getToken')
      && !req.url.includes('registrationConfirm')
      && !req.url.includes('resetPassword')
      && !req.url.includes('forgetPassword')
      ) {
      req = this.addToken(req, this._authService.accessToken);
    } else {
      req = this.addToken_ForgotPassword(req);
    }


    this._authService.preLoader = true;
    return next.handle(req).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          this._authService.preLoader = false;
        }
      },
        (err: any) => {
          this._authService.preLoader = false;
        }),
      catchError(error => {
        if (error instanceof HttpErrorResponse && error.status == 400) {

          if (error.error.code == 400
            && !req.url.includes('getToken')
            && !req.url.includes('resetPassword')
            && !req.url.includes('forgetPassword')
            && !req.url.includes('registrationConfirm')) {
            return this.tokenRefreshHandler(req, next);
          }
          return throwError(error);
        } else if (error instanceof HttpErrorResponse && error.status === 401
          && !req.url.includes('resetPassword')
          && !req.url.includes('forgetPassword')
          && !req.url.includes('registrationConfirm')) {
          this.isRefreshing = false;

          return this.loginModelHandler(req, next);
        } else {
          return throwError(error);
        }
      }));
  }


  loginModelHandler(req: HttpRequest<any>, next: HttpHandler) {
    this._authService.setPopupSubject({
      show: true,
      route: ""
    });
    this._router.navigate(['auth/login'])
    return EMPTY;
  }

  tokenRefreshHandler(req: HttpRequest<any>, next: HttpHandler) {

    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);
      return this._authService.onRefreshToken().pipe(
        switchMap((token: any) => {
          this.isRefreshing = false;
          this.refreshTokenSubject.next(token['access_token']);
          return next.handle(this.addToken(req, token['access_token']));
        }),
        catchError(error => {
          this.isRefreshing = false;
          return this.refreshTokenSubject.pipe(
            filter(token => token != null),
            take(1),
            switchMap(accessToken => {
              return next.handle(this.addToken(req, accessToken));
            })
          );
        })
      );
    } else {
      return this.refreshTokenSubject.pipe(
        filter(token => token != null),
        take(1),
        switchMap(accessToken => {
          return next.handle(this.addToken(req, accessToken));
        })
      );
    }
  }


}
