
    import { FormBuilder, FormGroup } from '@angular/forms';
    import { Component, HostListener, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent  {

  myForm:FormGroup;
  disabled = false;
  ShowFilter = false;
  limitSelection = false;
  cities = [];
  selectedItems= [];
  dropdownSettings: any = {};

//   @HostListener('window:beforeunload')
//   userLogout() {
//       this._authService.onLogout().subscribe(res => {
//       localStorage.clear();
//       sessionStorage.clear();
//      }, err => {
//       sessionStorage.clear();
//     });
//  }


  constructor(private fb: FormBuilder, private _authService: AuthService,) {}

  ngOnInit() {}

  onItemSelect(item: any) {
      console.log('onItemSelect', item);
  }
  onSelectAll(items: any) {
      console.log('onSelectAll', items);
  }
  toogleShowFilter() {
      this.ShowFilter = !this.ShowFilter;
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { allowSearchFilter: this.ShowFilter });
  }

  handleLimitSelection() {
      if (this.limitSelection) {
          this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: 2 });
      } else {
          this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: null });
      }
  }
}
