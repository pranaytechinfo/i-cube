import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { HttpInterceptorService } from './util/http/http-interceptor.service';
import { General } from './util/common/general';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ToastrModule } from 'ngx-toastr';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { HeaderComponent } from './common/header/header.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';
import { FooterComponent } from './common/footer/footer.component';
import { ForgetComponent } from './auth/forget/forget.component';
import { MasterComponent } from './master/master.component';
import { AuthService } from './auth/auth.service';
import { LogoutComponent } from './auth/logout/logout.component';
import { PreloaderComponent } from './util/preloader/preloader.component';
import { FacetsComponent } from './common/facets/facets.component';
import { DataTableComponent } from './common/data-table/data-table.component';
import { PaginationComponent } from './common/pagination/pagination.component';
import { MultiDropdownComponent } from './common/multi-dropdown/multi-dropdown.component';


import { DashboardComponent } from './master/dashboard/dashboard.component';
import { PatentsComponent } from './master/modules/patents/patents.component';
import { CardProjectComponent } from './common/card-project/card-project.component';
import { HeaderProjComponent } from './common/header/header-proj/header-proj.component';
import { ListPatentsComponent } from './common/list-views/list-patents/list-patents.component';
import { PrimaryFacetsComponent } from './common/primary-facets/primary-facets.component';
import { HighlightTextPipe } from './util/pipes/highlight-text.pipe';
import { BasicSearchComponent } from './common/basic-search/basic-search.component';
import { GlobalSearchComponent } from './master/global-search/global-search.component';
import { GloablListComponent } from './common/list-views/gloabl-list/gloabl-list.component';
import { GlobalCardComponent } from './common/list-views/global-card/global-card.component';
import { GlobalFacetsComponent } from './common/global-facets/global-facets.component';
import { ExecutiveSummaryComponent } from './master/summary/executive-summary/executive-summary.component';
import { DonutChartComponent } from './charts/donut/donut-chart.component';

import { HighchartsChartModule } from 'highcharts-angular';
import { NgSelectModule } from '@ng-select/ng-select';

import { GraphsContainerComponent } from './common/graphs-container/graphs-container.component';
import { ColumnGraphComponent } from './common/graphs-container/column-graph/column-graph.component';
import { DonutGraphComponent } from './common/graphs-container/donut-graph/donut-graph.component';
import { MapGraphComponent } from './common/graphs-container/map-graph/map-graph.component';
import { BarGraphComponent } from './common/graphs-container/bar-graph/bar-graph.component';
import { SafeHtmlPipe } from './util/pipes/safe-html.pipe';
import { GlobalGraphBuilderComponent } from './common/global-graph-builder/global-graph-builder.component';
import { GlobalBarChartComponent } from './common/global-graph-builder/global-bar-chart/global-bar-chart.component';
import { ArrayFilterPipe } from './util/pipes/array-filter.pipe';
import { GlobalHeatMapComponent } from './common/global-graph-builder/global-heat-map/global-heat-map.component';
import { FormBarChartComponent } from './common/global-graph-builder/forms/form-bar-chart/form-bar-chart.component';
import { FormBarYearChartComponent } from './common/global-graph-builder/forms/form-bar-year-chart/form-bar-year-chart.component';
import { FormHeatmapChartComponent } from './common/global-graph-builder/forms/form-heatmap-chart/form-heatmap-chart.component';
import { FormHeatmapYearChartComponent } from './common/global-graph-builder/forms/form-heatmap-year-chart/form-heatmap-year-chart.component';
import { GlobalBubbleChartComponent } from './common/global-graph-builder/global-bubble-chart/global-bubble-chart.component';
import { HeatMapComponent } from './common/graphs-container/heat-map/heat-map.component';
import { ImageViewerComponent } from './image-viewer/image-viewer.component';


@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginComponent,
    ForgetComponent,
    MasterComponent,
    LogoutComponent,
    PreloaderComponent,
    FooterComponent,
    HeaderComponent,
    ResetPasswordComponent,
    DashboardComponent,
    PatentsComponent,
    FacetsComponent,
    DataTableComponent,
    CardProjectComponent,
    HeaderProjComponent,
    PaginationComponent,
    ListPatentsComponent,
    PrimaryFacetsComponent,
    MultiDropdownComponent,
    HighlightTextPipe,
    BasicSearchComponent,
    GlobalSearchComponent,
    GloablListComponent,
    GlobalCardComponent,
    GlobalFacetsComponent,
    ExecutiveSummaryComponent,
    DonutChartComponent,
    GraphsContainerComponent,
    ColumnGraphComponent,
    DonutGraphComponent,
    MapGraphComponent,
    BarGraphComponent,
    SafeHtmlPipe,
    GlobalGraphBuilderComponent,
    GlobalBarChartComponent,
    ArrayFilterPipe,
    GlobalHeatMapComponent,
    FormBarChartComponent,
    FormBarYearChartComponent,
    FormHeatmapChartComponent,
    FormHeatmapYearChartComponent,
    GlobalBubbleChartComponent,
    HeatMapComponent,
    ImageViewerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule, // required animations module
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    ToastrModule.forRoot(),
    TooltipModule.forRoot(),
    AccordionModule.forRoot(),
    TabsModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    NgxDatatableModule,
    HighchartsChartModule,
    NgSelectModule
  ],
  providers: [
    AuthService,
    General,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
