// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const target = 'DEV';
const app = {
  DEV: {
    brand: {
      name: 'MC-SPIDER',
      logo: './assets/images/logo.png',
      copyright: 'Copyright &copy; ' + new Date().getFullYear()
    },
    mailer: {
      bug: 'bug@domain.com',
      info: 'info@domain.com',
      contact: 'contact@domain.com',
    },
    error: {
      showErrorDialog: false,
      showToaster: true,
      title: 'Something went wrong!',
      retryMessage: `We're trying to fix the problem, it might take a few seconds... Please try after sometime.`,
      errorMessage: `Oops... Looks like something went wrong, You can write yor query to us we will help you in solving this issue.`
    },
    baseAuth: {
      username: 'testInstance',
      password: '2k121101_MCPL'
    },
    ouathURL: window.location.origin + "/intelligence_service/" || 'https://reports.mcresearch.co.in/intelligence_service/',
    //ouathURL:  'http://178.79.139.23/intelligence_service/'

    barActionButtonStatus:true




  }
};

export const environment = {
  production: true,
  app: app[target]
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
